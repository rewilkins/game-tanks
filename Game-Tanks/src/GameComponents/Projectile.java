package GameComponents;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Projectile {
	// each projectile has its own shell picture
	JLabel jl_cannonShell = new JLabel(new ImageIcon("Resources\\Images\\Ammo\\CannonShell.png"));
	
	double locX;
	double locY;
	double projectAngle;
	boolean doneDamage;
	
	JLabel tempLabel1_1;  // temp label needed for the timers...
	JLabel tempLabel1_2;  // temp label needed for the timers...
	JLabel tempLabel1_3;  // temp label needed for the timers...
	
	// each projectile has its own Hit Smoke effects picture
	Timer timerHitSmoke_1 = new Timer();
	Timer timerHitSmoke_2 = new Timer();
	Timer timerHitSmoke_3 = new Timer();
	JLabel jl_HitCloud1 = new JLabel(new ImageIcon("Resources\\Images\\Effects\\Tank Hit Cloud1.png"));
	JLabel jl_HitCloud2 = new JLabel(new ImageIcon("Resources\\Images\\Effects\\Tank Hit Cloud2.png"));
	JLabel jl_HitCloud3 = new JLabel(new ImageIcon("Resources\\Images\\Effects\\Tank Hit Cloud3.png"));
	
	// init projectile components
	Projectile(JPanel panel, Vector<JLabel> effectsCollection){
		jl_cannonShell.setBounds(100, 600, 10, 10);
		panel.add(jl_cannonShell);
		locX = jl_cannonShell.getLocation().x;
		locY = jl_cannonShell.getLocation().y;
		jl_cannonShell.setVisible(false);
		doneDamage = false;
		
		jl_HitCloud1.setVisible(false);
		jl_HitCloud2.setVisible(false);
		jl_HitCloud3.setVisible(false);
		jl_HitCloud1.setBounds(0, 0, 125, 100);
		jl_HitCloud2.setBounds(0, 0, 125, 100);
		jl_HitCloud3.setBounds(0, 0, 125, 100);
		effectsCollection.add(jl_HitCloud1);
		effectsCollection.add(jl_HitCloud2);
		effectsCollection.add(jl_HitCloud3);
		panel.add(jl_HitCloud1);
		panel.add(jl_HitCloud2);
		panel.add(jl_HitCloud3);
		
	}
		
	// this function places the shell at the tip of the cannon based off of which angle the cannon is at
	public void loadCannonShell(double startLocX, double startLocY, double angle){
		if(angle == 0.0) jl_cannonShell.setLocation((int) startLocX + 152, (int) startLocY + 53);
		if(angle == 11.25) jl_cannonShell.setLocation((int) startLocX + 151, (int) startLocY + 41);
		if(angle == 22.5) jl_cannonShell.setLocation((int) startLocX + 147, (int) startLocY + 30);
		if(angle == 33.75) jl_cannonShell.setLocation((int) startLocX + 141, (int) startLocY + 20);
		if(angle == 45.0) jl_cannonShell.setLocation((int) startLocX + 132, (int) startLocY + 11);
		if(angle == 56.25) jl_cannonShell.setLocation((int) startLocX + 121, (int) startLocY + 3);
		if(angle == 67.5) jl_cannonShell.setLocation((int) startLocX + 106, (int) startLocY - 3);
		if(angle == 78.75) jl_cannonShell.setLocation((int) startLocX + 91, (int) startLocY - 7);
		if(angle == 90.0) jl_cannonShell.setLocation((int) startLocX + 75, (int) startLocY - 8);
		if(angle == 101.25) jl_cannonShell.setLocation((int) startLocX + 58, (int) startLocY - 6);
		if(angle == 112.5) jl_cannonShell.setLocation((int) startLocX + 43, (int) startLocY - 2);
		if(angle == 123.75) jl_cannonShell.setLocation((int) startLocX + 30, (int) startLocY + 4);
		if(angle == 135.0) jl_cannonShell.setLocation((int) startLocX + 19, (int) startLocY + 11);
		if(angle == 146.25) jl_cannonShell.setLocation((int) startLocX + 10, (int) startLocY + 21);
		if(angle == 157.5) jl_cannonShell.setLocation((int) startLocX + 3, (int) startLocY + 31);
		if(angle == 168.75) jl_cannonShell.setLocation((int) startLocX + 0, (int) startLocY + 42);
		if(angle == 180.0) jl_cannonShell.setLocation((int) startLocX - 3, (int) startLocY + 53);
		if(angle == 191.25) jl_cannonShell.setLocation((int) startLocX - 2, (int) startLocY + 65);
		if(angle == 202.5) jl_cannonShell.setLocation((int) startLocX + 3, (int) startLocY + 77);
		if(angle == 213.75) jl_cannonShell.setLocation((int) startLocX + 10, (int) startLocY + 88);
		if(angle == 225.0) jl_cannonShell.setLocation((int) startLocX + 20, (int) startLocY + 96);
		if(angle == 236.25) jl_cannonShell.setLocation((int) startLocX + 30, (int) startLocY + 104);
		if(angle == 247.5) jl_cannonShell.setLocation((int) startLocX + 44, (int) startLocY + 110);
		if(angle == 258.75) jl_cannonShell.setLocation((int) startLocX + 60, (int) startLocY + 113);
		if(angle == 270.0) jl_cannonShell.setLocation((int) startLocX + 75, (int) startLocY + 114);
		if(angle == 281.25) jl_cannonShell.setLocation((int) startLocX + 91, (int) startLocY + 113);
		if(angle == 292.5) jl_cannonShell.setLocation((int) startLocX + 108, (int) startLocY + 110);
		if(angle == 303.75) jl_cannonShell.setLocation((int) startLocX + 120, (int) startLocY + 104);
		if(angle == 315.0) jl_cannonShell.setLocation((int) startLocX + 131, (int) startLocY + 96);
		if(angle == 326.25) jl_cannonShell.setLocation((int) startLocX + 141, (int) startLocY + 87);
		if(angle == 337.5) jl_cannonShell.setLocation((int) startLocX + 147, (int) startLocY + 76);
		if(angle == 348.75) jl_cannonShell.setLocation((int) startLocX + 153, (int) startLocY + 65);
			
		locX = jl_cannonShell.getLocation().x;
		locY = jl_cannonShell.getLocation().y;
		jl_cannonShell.setVisible(true);
	}
		
	// this function controls the movement of the projectile after it has been shot
	public void project(final double angle){
		
		projectAngle = angle;
		
		// this timer moves the projectile across the screen.
		// it is important that the timer stops before this 
		// projectile is used again otherwise the projection speed
		// will double with each click.
		final Timer timer1 = new Timer();
		timer1.schedule(new TimerTask() { 
			private int stopAnimation = 300;  //100 = 5 seconds.  6 seconds is enough time for the ball to clear the screen
			@Override
			public void run() {

				if(projectAngle == 0.0){
					locX += 6;
					locY -= 0;
				}
				if(projectAngle == 11.25){
					locX += 6;
					locY -= 1;
				}
				if(projectAngle == 22.5){
					locX += 5;
					locY -= 2;
				}
				if(projectAngle == 33.75){
					locX += 5;
					locY -= 3;
				}
				if(projectAngle == 45.0){
					locX += 4;
					locY -= 4;
				}
				if(projectAngle == 56.25){
					locX += 3;
					locY -= 4;
				}
				if(projectAngle == 67.5){
					locX += 2;
					locY -= 5;
				}
				if(projectAngle == 78.75){
					locX += 1;
					locY -= 5;
				}
				if(projectAngle == 90.0){
					locX += 0;
					locY -= 5;
				}
				if(projectAngle == 101.25){
					locX -= 1;
					locY -= 5;
				}
				if(projectAngle == 112.5){
					locX -= 2;
					locY -= 5;
				}
				if(projectAngle == 123.75){
					locX -= 3;
					locY -= 4;
				}
				if(projectAngle == 135.0){
					locX -= 4;
					locY -= 4;
				}
				if(projectAngle == 146.25){
					locX -= 5;
					locY -= 3;
				}
				if(projectAngle == 157.5){
					locX -= 5;
					locY -= 2;
				}
				if(projectAngle == 168.75){
					locX -= 6;
					locY -= 1;
				}
				if(projectAngle == 180.0){
					locX -= 6;
					locY -= 0;
				}
				if(projectAngle == 191.25){
					locX -= 6;
					locY += 1;
				}
				if(projectAngle == 202.5){
					locX -= 5;
					locY += 2;
				}
				if(projectAngle == 213.75){
					locX -= 5;
					locY += 3;
				}
				if(projectAngle == 225.0){
					locX -= 4;
					locY += 4;
				}
				if(projectAngle == 236.25){
					locX -= 3;
					locY += 4;
				}
				if(projectAngle == 247.5){
					locX -= 2;
					locY += 5;
				}
				if(projectAngle == 258.75){
					locX -= 1;
					locY += 5;
				}
				if(projectAngle == 270.0){
					locX -= 0;
					locY += 5;
				}
				if(projectAngle == 281.25){
					locX += 1;
					locY += 5;
				}
				if(projectAngle == 292.5){
					locX += 2;
					locY += 5;
				}
				if(projectAngle == 303.75){
					locX += 3;
					locY += 4;
				}
				if(projectAngle == 315.0){
					locX += 4;
					locY += 4;
				}
				if(projectAngle == 326.25){
					locX += 5;
					locY += 3;
				}
				if(projectAngle == 337.5){
					locX += 5;
					locY += 2;
				}
				if(projectAngle == 348.75){
					locX += 6;
					locY += 1;
				}
				
				jl_cannonShell.setLocation((int) locX, (int) locY);
				if (--stopAnimation <= 0){
					jl_cannonShell.setVisible(false);
					doneDamage = true;
					jl_cannonShell.setLocation((int) locX, (int) locY);
					timer1.cancel();
				}	
			}
		}, 0, 25);  // the lower the second number the faster the projectile moves
	}
	
	// this function controls the explosion animations on collision
	public void explode(JLabel label100, JLabel label66, JLabel label33){
		tempLabel1_1 = label100;
		tempLabel1_2 = label66;
		tempLabel1_3 = label33;
		
		// this timer nest controls the hit smoke fade effect
		// this effect is based off of 3 timers.
		timerHitSmoke_1.schedule(new TimerTask() {
			@Override
			public void run() {
				
				tempLabel1_1.setVisible(false);  // hide 100% transparency
				
				timerHitSmoke_2.schedule(new TimerTask() {   
					@Override
					public void run() {
						
						tempLabel1_2.setVisible(false);  // hide 66% transparency
						
						timerHitSmoke_3.schedule(new TimerTask() {   
							@Override
							public void run() {
								
								tempLabel1_3.setVisible(false);  // hide 33% transparency
								
							}
						}, 300);
						
					}
				}, 300);
						
			}
		}, 300);
	}
	
	
	
	
}
