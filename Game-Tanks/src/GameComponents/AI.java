package GameComponents;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class AI {

	boolean playerHeard = false;
	boolean timerSet = false;
	int AImovingStages = 1;
	
	public AI(final Vector<Tank> tanks){

		
		// this timer controls the AI
		final Timer timer_AI = new Timer();
		timer_AI.schedule(new TimerTask() {   
			@Override
			public void run() {
				if(tanks.elementAt(0).alive == true && tanks.elementAt(1).paused == false){
					if(tanks.elementAt(1).alive == true){
					
					
						//find angle between you and target
						double playerX = tanks.elementAt(0).locX + 80;
						double playerY = tanks.elementAt(0).locY + 58;
						double enemyX = tanks.elementAt(1).locX + 80;
						double enemyY = tanks.elementAt(1).locY + 58;
	
						double adj = 0;  // Adjacent edge
						double hyp = 0;  // Hypotenuse
						
						// calc triangle to get angle from tank to mouse
						adj = Math.sqrt((Math.pow((playerX - enemyX),2)) + (Math.pow((0),2)));
						hyp = Math.sqrt((Math.pow((playerX - enemyX),2)) + (Math.pow((playerY - enemyY),2)));
						// calc angle from triangle
						double angle = Math.acos(adj / hyp);
						angle = Math.toDegrees(angle);  // puts rads into degrees
						
						// mod angle result to get appropriate angle in other quadrants
						if(enemyX >= playerX && enemyY < playerY){}  // Quadrant 1 - no mods
						if(enemyX < playerX && enemyY <= playerY) angle = (90 - (angle - 90));  // Quadrant 2
						if(enemyX <= playerX && enemyY > playerY) angle = (180 + angle);  // Quadrant 3
						if(enemyX > playerX && enemyY >= playerY) angle = (270 - (angle - 90));  // Quadrant 4
						// end find angle
						
						// find range distance at that angle
						double r = 0;  // radius
						double a = 1500 / 2;  // horizontal distance of ellipse
						double b = 750 / 2;  // vertical distance of ellipse
						
						r = (a*b)/(Math.sqrt((Math.pow(a, 2)*Math.pow(Math.sin(angle), 2)) + (Math.pow(b, 2)*Math.pow(Math.cos(angle), 2))));
						// end find range distance
						
						
						// find actual distance
						double actualDist = 0;
						actualDist = Math.sqrt((Math.pow((playerX - enemyX),2)) + (Math.pow((playerY - enemyY),2)));
						// end find actual distance
	
						
						/*
						// MOVING AI (NON FUNCTIONING YET)
						int modifyer = 0;
						if(tanks.elementAt(0).currentSector == 1  && tanks.elementAt(0).currentSector == 3) modifyer = 3000;
						if(tanks.elementAt(0).currentSector == 2  && tanks.elementAt(0).currentSector == 3) modifyer = 1500;
						if(tanks.elementAt(0).currentSector == 3  && tanks.elementAt(0).currentSector == 3) modifyer = 0;
						
						if(tanks.elementAt(0).currentSector == 1  && tanks.elementAt(0).currentSector == 2) modifyer = 3000;
						if(tanks.elementAt(0).currentSector == 2  && tanks.elementAt(0).currentSector == 2) modifyer = 1500;
						if(tanks.elementAt(0).currentSector == 3  && tanks.elementAt(0).currentSector == 2) modifyer = 0;
						
						
						if(AImovingStages == 1){
							tanks.elementAt(1).angleBottom = 225;
							tanks.elementAt(1).powerLevel = 1;
							tanks.elementAt(1).jl_TankBottom000_00.setVisible(false);
							tanks.elementAt(1).jl_TankBottom225_00.setVisible(true);
							if(tanks.elementAt(1).locY > 665) AImovingStages = 2;
						}
						if(AImovingStages == 2){
							tanks.elementAt(1).angleBottom = 180;
							tanks.elementAt(1).powerLevel = 2;
							tanks.elementAt(1).jl_TankBottom225_00.setVisible(false);
							tanks.elementAt(1).jl_TankBottom180_00.setVisible(true);
							if(tanks.elementAt(1).locX < 989+modifyer) AImovingStages = 3;
						}
						if(AImovingStages == 3){
							tanks.elementAt(1).angleBottom = 135;
							tanks.elementAt(1).powerLevel = 2;
							tanks.elementAt(1).jl_TankBottom180_00.setVisible(false);
							tanks.elementAt(1).jl_TankBottom135_00.setVisible(true);
							if(tanks.elementAt(1).locY < 450) AImovingStages = 4;
						}
						if(AImovingStages == 4){
							tanks.elementAt(1).angleBottom = 180;
							tanks.elementAt(1).powerLevel = 2;
							tanks.elementAt(1).jl_TankBottom135_00.setVisible(false);
							tanks.elementAt(1).jl_TankBottom180_00.setVisible(true);
							if(tanks.elementAt(1).locX < 500+modifyer) AImovingStages = 5;
						}
						if(AImovingStages == 5){
							tanks.elementAt(1).angleBottom = 225;
							tanks.elementAt(1).powerLevel = 2;
							tanks.elementAt(1).jl_TankBottom180_00.setVisible(false);
							tanks.elementAt(1).jl_TankBottom225_00.setVisible(true);
							if(tanks.elementAt(1).locX < 293+modifyer) AImovingStages = 6;
						}
						if(AImovingStages == 6){
							tanks.elementAt(1).angleBottom = 180;
							tanks.elementAt(1).powerLevel = 3;
							tanks.elementAt(1).jl_TankBottom225_00.setVisible(false);
							tanks.elementAt(1).jl_TankBottom180_00.setVisible(true);
							if(tanks.elementAt(1).locX < 0+modifyer) AImovingStages = 7;
						}
						*/
						
						
						
						
						
						// ATTACKING AI
						// in reaction to hearing a shot 
						if(tanks.elementAt(1).paused == false){
							if(tanks.elementAt(0).canShootCannon == false) playerHeard = true;
							if(playerHeard == true && tanks.elementAt(0).currentSector == tanks.elementAt(1).currentSector){
								// open fire
								tanks.elementAt(1).rotateTurret(tanks.elementAt(0).locX + 80, tanks.elementAt(0).locY + 80);
								tanks.elementAt(1).shoot(tanks.elementAt(0).locX + 80, tanks.elementAt(0).locY + 58);
								
								if(timerSet == false){
									timerSet = true;
									//timer allows for this reaction to happen for certain number of seconds
									final Timer reactionEndTimer = new Timer();
									reactionEndTimer.schedule(new TimerTask() {   
										@Override
										public void run() {
											timerSet = false;
											playerHeard = false;
										}
									}, 5000);  // 5000 =  reaction is active for ~5 seconds
								}
								
							}
						}
						// end reaction
						
						
						// handle player in range event
						if(actualDist < r && playerHeard == false && tanks.elementAt(0).currentSector == tanks.elementAt(1).currentSector){
							tanks.elementAt(1).rotateTurret(tanks.elementAt(0).locX + 80, tanks.elementAt(0).locY + 80);
							tanks.elementAt(1).shoot(tanks.elementAt(0).locX + 80, tanks.elementAt(0).locY + 58);
						}
						// end handle player in range event
					}
				}
			}
		}, 0, 10); // the higher second number reduces excessive rotating actions which reduced tank flickering
		
		
		
	}
	
	
}
