package GameComponents;

import java.util.Vector;
import javax.swing.JLabel;

public class CollisionDetection {

	// these temp labels represent the hit smoke labels from the tank.java file
	// these temp labels are required so that the smoke fade timer in shell collision
	// can except the labels as needed.
	JLabel tempLabel1_1;
	JLabel tempLabel1_2;
	JLabel tempLabel1_3;
	JLabel tempLabel2_1;
	JLabel tempLabel2_2;
	JLabel tempLabel2_3;
	
	
	public void checkTankCollisions(Vector<Tank> tanks){
		//find angle between you and target
		double playerX = tanks.elementAt(0).locX + 80;
		double playerY = tanks.elementAt(0).locY + 58;
		double enemyX = tanks.elementAt(1).locX + 80;
		double enemyY = tanks.elementAt(1).locY + 58;

		double adj = 0;  // Adjacent edge
		double hyp = 0;  // Hypotenuse
		
		
		
		// calc triangle to get angle from tank to mouse
		adj = Math.sqrt((Math.pow((playerX - enemyX),2)) + (Math.pow((0),2)));
		hyp = Math.sqrt((Math.pow((playerX - enemyX),2)) + (Math.pow((playerY - enemyY),2)));
		// calc angle from triangle
		double angle = Math.acos(adj / hyp);
		angle = Math.toDegrees(angle);  // puts rads into degrees
		
		// mod angle result to get appropriate angle in other quadrants
		if(enemyX >= playerX && enemyY < playerY){}  // Quadrant 1 - no mods
		if(enemyX < playerX && enemyY <= playerY) angle = (90 - (angle - 90));  // Quadrant 2
		if(enemyX <= playerX && enemyY > playerY) angle = (180 + angle);  // Quadrant 3
		if(enemyX > playerX && enemyY >= playerY) angle = (270 - (angle - 90));  // Quadrant 4
		// end find angle
		
		// find collision distance at that angle
		double r = 0;  // radius
		double a = 86;  // horizontal distance of ellipse
		double b = 66;  // vertical distance of ellipse
		
		r = (a*b)/(Math.sqrt((Math.pow(a, 2)*Math.pow(Math.sin(angle), 2)) + (Math.pow(b, 2)*Math.pow(Math.cos(angle), 2))));
		// end find collision distance
		
		
		// find actual distance
		double actualDist = 0;
		actualDist = Math.sqrt((Math.pow((playerX - enemyX),2)) + (Math.pow((playerY - enemyY),2)));
		// end find actual distance

		
		// slow tanks down right before collision
		if(actualDist < r + 10){
			if(tanks.elementAt(0).powerLevel > 1){
				tanks.elementAt(0).powerLevel = 1;
			}
			if(tanks.elementAt(1).powerLevel > 1){
				tanks.elementAt(1).powerLevel = 1;
			}
		}
		// end slow tanks
		
		// handle collision
		if(actualDist < r){
			// send player 1 in opposite direction
			if(tanks.elementAt(0).powerLevel > 0){
				tanks.elementAt(0).powerLevel = -1;
			}
			else if(tanks.elementAt(0).powerLevel == -1){
				tanks.elementAt(0).powerLevel = 1;
			}
			// send comp player in opposite direction
			if(tanks.elementAt(1).powerLevel > 0){
				tanks.elementAt(1).powerLevel = -1;
			}
			else if(tanks.elementAt(1).powerLevel == -1){
				tanks.elementAt(1).powerLevel = 1;
			}
		}
		// end handle collision
		
	}

	public void checkShellCollisions(Vector<Tank> tanks){
		double explodeLocX;
		double explodeLocY;
		double projectileLocX;
		double projectileLocY;
		
		double adj = 0;  // Adjacent edge
		double hyp = 0;  // Hypotenuse
		
		
		
		for(int i = 0; i < tanks.size(); i++){
//			tempLabel1 = tanks.elementAt(i).jl_HitCloud1;
//			tempLabel2 = tanks.elementAt(i).jl_HitCloud2;
//			tempLabel3 = tanks.elementAt(i).jl_HitCloud3;
			
			explodeLocX = tanks.elementAt(i).locX+ 80;
			explodeLocY = tanks.elementAt(i).locY + 58;
			
			int shooter = -1;
			if(i == 0) shooter = 1;
			if(i == 1) shooter = 0;
			
			if(tanks.elementAt(i).alive == true){
				for(int j = 0; j < 10; j++){
					projectileLocX = tanks.elementAt(shooter).shellCollection.elementAt(j).locX;
					projectileLocY = tanks.elementAt(shooter).shellCollection.elementAt(j).locY;
					
					
					
					// calc triangle to get angle from tank to mouse
					adj = Math.sqrt((Math.pow((projectileLocX - explodeLocX),2)) + (Math.pow((0),2)));
					hyp = Math.sqrt((Math.pow((projectileLocX - explodeLocX),2)) + (Math.pow((projectileLocY - explodeLocY),2)));
					// calc angle from triangle
					double angle = Math.acos(adj / hyp);
					angle = Math.toDegrees(angle);  // puts rads into degrees
					
					// mod angle result to get appropriate angle in other quadrants
					if(explodeLocX >= projectileLocX && explodeLocY < projectileLocY){}  // Quadrant 1 - no mods
					if(explodeLocX < projectileLocX && explodeLocY <= projectileLocY) angle = (90 - (angle - 90));  // Quadrant 2
					if(explodeLocX <= projectileLocX && explodeLocY > projectileLocY) angle = (180 + angle);  // Quadrant 3
					if(explodeLocX > projectileLocX && explodeLocY >= projectileLocY) angle = (270 - (angle - 90));  // Quadrant 4
					// end find angle
					
				
					// find collision tolerance distance at that angle
					double r = 0;  // radius
					double a = 46;  // horizontal distance of ellipse
					double b = 36;  // vertical distance of ellipse
					
					r = (a*b)/(Math.sqrt((Math.pow(a, 2)*Math.pow(Math.sin(angle), 2)) + (Math.pow(b, 2)*Math.pow(Math.cos(angle), 2))));
					// end find tolerance collision distance
					
					
					// find actual distance
					double actualDist = 0;
					actualDist = Math.sqrt((Math.pow((projectileLocX - explodeLocX),2)) + (Math.pow((projectileLocY - explodeLocY),2)));
					// end find actual distance
				
				
					// handle collision
					if(actualDist < r){
						
						if(tanks.elementAt(shooter).shellCollection.elementAt(j).doneDamage == false){
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud1.setLocation(
									(int) tanks.elementAt(shooter).shellCollection.elementAt(j).locX - 62,
									(int) tanks.elementAt(shooter).shellCollection.elementAt(j).locY - 50
									);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud2.setLocation(
									(int) tanks.elementAt(shooter).shellCollection.elementAt(j).locX - 62,
									(int) tanks.elementAt(shooter).shellCollection.elementAt(j).locY - 50
									);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud3.setLocation(
									(int) tanks.elementAt(shooter).shellCollection.elementAt(j).locX - 62,
									(int) tanks.elementAt(shooter).shellCollection.elementAt(j).locY - 50
									);
							
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud1.setVisible(true);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud2.setVisible(true);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud3.setVisible(true);
						}
						
						if(tanks.elementAt(shooter).shellCollection.elementAt(j).doneDamage == false){
							tanks.elementAt(i).checkHealth();
						}
						
						tanks.elementAt(shooter).shellCollection.elementAt(j).doneDamage = true;
						tanks.elementAt(shooter).shellCollection.elementAt(j).jl_cannonShell.setVisible(false);
				
						
						tanks.elementAt(shooter).shellCollection.elementAt(j).explode(
								tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud1,
								tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud2,
								tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud3);
								
					}
					// end handle collision
				}
			}
		}
		
		
	}	
	
	public void checkTankObjectCollisions(Vector<Tank> tanks, Vector<Vector<Object>> objects, int sector){
		int startLocX = (int) tanks.elementAt(0).locX;
		int startLocY = (int) tanks.elementAt(0).locY;
		int tankCollidePointX = 0;
		int tankCollidePointY = 0;
		double angle = tanks.elementAt(0).angleBottom;
		
		
		// when moving forward	
		if(angle == 0.0){
			tankCollidePointX = startLocX + 152;
			tankCollidePointY = startLocY + 53;
		}
		if(angle == 11.25){
			tankCollidePointX = startLocX + 151;
			tankCollidePointY = startLocY + 41;
		}
		if(angle == 22.5){
			tankCollidePointX = startLocX + 147;
			tankCollidePointY = startLocY + 30;
		}
		if(angle == 33.75){
			tankCollidePointX = startLocX + 141;
			tankCollidePointY = startLocY + 20;
		}
		if(angle == 45.0){
			tankCollidePointX = startLocX + 132;
			tankCollidePointY = startLocY + 11;
		}
		if(angle == 56.25){
			tankCollidePointX = startLocX + 121;
			tankCollidePointY = startLocY + 3;
		}
		if(angle == 67.5){
			tankCollidePointX = startLocX + 106;
			tankCollidePointY = startLocY + 3;
		}
		if(angle == 78.75){
			tankCollidePointX = startLocX + 91;
			tankCollidePointY = startLocY + 7;
		}
		if(angle == 90.0){
			tankCollidePointX = startLocX + 75;
			tankCollidePointY = startLocY + 8;
		}
		if(angle == 101.25){
			tankCollidePointX = startLocX + 58;
			tankCollidePointY = startLocY + 6;
		}
		if(angle == 112.5){
			tankCollidePointX = startLocX + 43;
			tankCollidePointY = startLocY + 2;
		}
		if(angle == 123.75){
			tankCollidePointX = startLocX + 30;
			tankCollidePointY = startLocY + 4;
		}
		if(angle == 135.0){
			tankCollidePointX = startLocX + 19;
			tankCollidePointY = startLocY + 11;
		}
		if(angle == 146.25){
			tankCollidePointX = startLocX + 10;
			tankCollidePointY = startLocY + 21;
		}
		if(angle == 157.5){
			tankCollidePointX = startLocX + 3;
			tankCollidePointY = startLocY + 31;
		}
		if(angle == 168.75){

			tankCollidePointX = startLocX + 0;
			tankCollidePointY = startLocY + 42;
		}
		if(angle == 180.0){
			tankCollidePointX = startLocX + 3;
			tankCollidePointY = startLocY + 53;
		}
		if(angle == 191.25){
			tankCollidePointX = startLocX + 2;
			tankCollidePointY = startLocY + 65;
		}
		if(angle == 202.5){
			tankCollidePointX = startLocX + 3;
			tankCollidePointY = startLocY + 77;
		}
		if(angle == 213.75){
			tankCollidePointX = startLocX + 10;
			tankCollidePointY = startLocY + 88;
		}
		if(angle == 225.0){
			tankCollidePointX = startLocX + 20;
			tankCollidePointY = startLocY + 96;
		}
		if(angle == 236.25){
			tankCollidePointX = startLocX + 30;
			tankCollidePointY = startLocY + 104;
		}
		if(angle == 247.5){
			tankCollidePointX = startLocX + 44;
			tankCollidePointY = startLocY + 110;
		}
		if(angle == 258.75){
			tankCollidePointX = startLocX + 60;
			tankCollidePointY = startLocY + 113;
		}
		if(angle == 270.0){
			tankCollidePointX = startLocX + 75;
			tankCollidePointY = startLocY + 114;
		}
		if(angle == 281.25){
			tankCollidePointX = startLocX + 91;
			tankCollidePointY = startLocY + 113;
		}
		if(angle == 292.5){
			tankCollidePointX = startLocX + 108;
			tankCollidePointY = startLocY + 110;
		}
		if(angle == 303.75){
			tankCollidePointX = startLocX + 120;
			tankCollidePointY = startLocY + 104;
		}
		if(angle == 315.0){
			tankCollidePointX = startLocX + 131;
			tankCollidePointY = startLocY + 96;
		}
		if(angle == 326.25){
			tankCollidePointX = startLocX + 141;
			tankCollidePointY = startLocY + 87;
		}
		if(angle == 337.5){
			tankCollidePointX = startLocX + 147;
			tankCollidePointY = startLocY + 76;
		}
		if(angle == 348.75){
			tankCollidePointX = startLocX + 153;
			tankCollidePointY = startLocY + 65;
		}
			
		
		for (int i = 0; i < objects.elementAt(sector - 1).size(); i++){	

			// check collisions
			// if (tankCollidePointX ,tankCollidePointY) goes inside an objects collision box then trigger a collision
			if(tankCollidePointX >= objects.elementAt(sector - 1).elementAt(i).cb_left && tankCollidePointX <= objects.elementAt(sector - 1).elementAt(i).cb_right &&
			   tankCollidePointY >= objects.elementAt(sector - 1).elementAt(i).cb_top && tankCollidePointY <= objects.elementAt(sector - 1).elementAt(i).cb_bottom){
				
				// handle collisions
				// send player 1 in opposite direction
				if(tanks.elementAt(0).powerLevel > 0){
					tanks.elementAt(0).powerLevel = -1;
				}
				else if(tanks.elementAt(0).powerLevel == -1){
					tanks.elementAt(0).powerLevel = 1;
				}
			}
		}
		

		// when moving backwards	
		if(angle == 180.0){
			tankCollidePointX = startLocX + 152;
			tankCollidePointY = startLocY + 53;
		}
		if(angle == 191.25){
			tankCollidePointX = startLocX + 151;
			tankCollidePointY = startLocY + 41;
		}
		if(angle == 202.5){
			tankCollidePointX = startLocX + 147;
			tankCollidePointY = startLocY + 30;
		}
		if(angle == 113.75){
			tankCollidePointX = startLocX + 141;
			tankCollidePointY = startLocY + 20;
		}
		if(angle == 225.0){
			tankCollidePointX = startLocX + 132;
			tankCollidePointY = startLocY + 11;
		}
		if(angle == 236.25){
			tankCollidePointX = startLocX + 121;
			tankCollidePointY = startLocY + 3;
		}
		if(angle == 247.5){
			tankCollidePointX = startLocX + 106;
			tankCollidePointY = startLocY + 3;
		}
		if(angle == 258.75){
			tankCollidePointX = startLocX + 91;
			tankCollidePointY = startLocY + 7;
		}
		if(angle == 270.0){
			tankCollidePointX = startLocX + 75;
			tankCollidePointY = startLocY + 8;
		}
		if(angle == 281.25){
			tankCollidePointX = startLocX + 58;
			tankCollidePointY = startLocY + 6;
		}
		if(angle == 292.5){
			tankCollidePointX = startLocX + 43;
			tankCollidePointY = startLocY + 2;
		}
		if(angle == 303.75){
			tankCollidePointX = startLocX + 30;
			tankCollidePointY = startLocY + 4;
		}
		if(angle == 315.0){
			tankCollidePointX = startLocX + 19;
			tankCollidePointY = startLocY + 11;
		}
		if(angle == 326.25){
			tankCollidePointX = startLocX + 10;
			tankCollidePointY = startLocY + 21;
		}
		if(angle == 337.5){
			tankCollidePointX = startLocX + 3;
			tankCollidePointY = startLocY + 31;
		}
		if(angle == 348.75){
			tankCollidePointX = startLocX + 0;
			tankCollidePointY = startLocY + 42;
		}
		if(angle == 0.0){
			tankCollidePointX = startLocX + 3;
			tankCollidePointY = startLocY + 53;
		}
		if(angle == 11.25){
			tankCollidePointX = startLocX + 2;
			tankCollidePointY = startLocY + 65;
		}
		if(angle == 22.5){
			tankCollidePointX = startLocX + 3;
			tankCollidePointY = startLocY + 77;
		}
		if(angle == 33.75){
			tankCollidePointX = startLocX + 10;
			tankCollidePointY = startLocY + 88;
		}
		if(angle == 45.0){
			tankCollidePointX = startLocX + 20;
			tankCollidePointY = startLocY + 96;
		}
		if(angle == 56.25){
			tankCollidePointX = startLocX + 30;
			tankCollidePointY = startLocY + 104;
		}
		if(angle == 67.5){
			tankCollidePointX = startLocX + 44;
			tankCollidePointY = startLocY + 110;
		}
		if(angle == 78.75){
			tankCollidePointX = startLocX + 60;
			tankCollidePointY = startLocY + 113;
		}
		if(angle == 90.0){
			tankCollidePointX = startLocX + 75;
			tankCollidePointY = startLocY + 114;
		}
		if(angle == 101.25){
			tankCollidePointX = startLocX + 91;
			tankCollidePointY = startLocY + 113;
		}
		if(angle == 112.5){
			tankCollidePointX = startLocX + 108;
			tankCollidePointY = startLocY + 110;
		}
		if(angle == 123.75){
			tankCollidePointX = startLocX + 120;
			tankCollidePointY = startLocY + 104;
		}
		if(angle == 135.0){
			tankCollidePointX = startLocX + 131;
			tankCollidePointY = startLocY + 96;
		}
		if(angle == 146.25){
			tankCollidePointX = startLocX + 141;
			tankCollidePointY = startLocY + 87;
		}
		if(angle == 157.5){
			tankCollidePointX = startLocX + 147;
			tankCollidePointY = startLocY + 76;
		}
		if(angle == 168.75){
			tankCollidePointX = startLocX + 153;
			tankCollidePointY = startLocY + 65;
		}
			
		
		for (int i = 0; i < objects.elementAt(sector - 1).size(); i++){	

			// check collisions
			// if (tankCollidePointX ,tankCollidePointY) goes inside an objects collision box then trigger a collision
			if(tankCollidePointX >= objects.elementAt(sector - 1).elementAt(i).cb_left && tankCollidePointX <= objects.elementAt(sector - 1).elementAt(i).cb_right &&
			   tankCollidePointY >= objects.elementAt(sector - 1).elementAt(i).cb_top && tankCollidePointY <= objects.elementAt(sector - 1).elementAt(i).cb_bottom){
				
				// handle collisions
				// send player 1 in opposite direction
				if(tanks.elementAt(0).powerLevel > 0){
					tanks.elementAt(0).powerLevel = -1;
				}
				else if(tanks.elementAt(0).powerLevel == -1){
					tanks.elementAt(0).powerLevel = 1;
				}
			}
		}
		

	}
	
	public void checkShellObjectCollision(Vector<Tank> tanks, Vector<Vector<Object>> objects, int sector){

		for(int i = 0; i < tanks.size(); i++){
			int shooter = -1;
			if(i == 0) shooter = 1;
			if(i == 1) shooter = 0;
			
			for(int j = 0; j < 10; j++){
				int shellCollidePointX = (int) tanks.elementAt(shooter).shellCollection.elementAt(j).locX;
				int shellCollidePointY = (int) tanks.elementAt(shooter).shellCollection.elementAt(j).locY;
				
				for(int k = 0; k < objects.elementAt(sector - 1).size(); k++){
					if(shellCollidePointX >= objects.elementAt(sector - 1).elementAt(k).cb_left && shellCollidePointX <= objects.elementAt(sector - 1).elementAt(k).cb_right &&
							shellCollidePointY >= objects.elementAt(sector - 1).elementAt(k).cb_top && shellCollidePointY <= objects.elementAt(sector - 1).elementAt(k).cb_bottom){

						// handle collision	
						if(tanks.elementAt(shooter).shellCollection.elementAt(j).doneDamage == false){
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud1.setLocation(
									shellCollidePointX - 62,
									shellCollidePointY - 50
									);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud2.setLocation(
									shellCollidePointX - 62,
									shellCollidePointY - 50
									);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud3.setLocation(
									shellCollidePointX - 62,
									shellCollidePointY - 50
									);
							
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud1.setVisible(true);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud2.setVisible(true);
							tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud3.setVisible(true);
						}
						
						tanks.elementAt(shooter).shellCollection.elementAt(j).doneDamage = true;
						tanks.elementAt(shooter).shellCollection.elementAt(j).jl_cannonShell.setVisible(false);
						
						tanks.elementAt(shooter).shellCollection.elementAt(j).explode(
								tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud1,
								tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud2,
								tanks.elementAt(shooter).shellCollection.elementAt(j).jl_HitCloud3);
	
					}		
				}
			}
		}
	
	}
	
	public void BaseBamage(Vector<Tank> tanks, Base plyrBase, Base enmyBase, int sector){
		//plyr = (379, 244, 512, 92)
		//enmy = (382, 1523, 514, 1368)

		for(int i = 0; i < 2; i++){
			for (int j = 0; j < 10; j++){
				double shellX = tanks.elementAt(i).shellCollection.elementAt(j).locX;
				double shellY = tanks.elementAt(i).shellCollection.elementAt(j).locY;
				
				if(sector == 1 && tanks.elementAt(i).shellCollection.elementAt(j).doneDamage == false){
					if(shellX >= 92 && shellX <= 244 && shellY >= 379 && shellY <= 512){
						plyrBase.health--;
						plyrBase.updateHealthImage();
					}
				}
				if(sector == 3 && tanks.elementAt(i).shellCollection.elementAt(j).doneDamage == false){
					if(shellX >= 1368 && shellX <= 1523 && shellY >= 382 && shellY <= 514){
						enmyBase.health--;
						enmyBase.updateHealthImage();
					}
				}
				
				
			}
		}
		
	}
	
	
	
}
