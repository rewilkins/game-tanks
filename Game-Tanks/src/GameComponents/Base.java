package GameComponents;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Base {
	int health;
	
	boolean inited = false;
	
	// base's picture
	JLabel factory;
	
	// base's health pictures
	JLabel Health5;
	JLabel Health4;
	JLabel Health3;
	JLabel Health2;
	JLabel Health1;
	JLabel HealthDEAD;

	// init base
	public Base(JPanel panel, int X, int Y){
		health = 5;
		inited = true;
		
		// load base's picture
		factory = new JLabel(new ImageIcon("Resources\\Images\\Base\\Starcraft_Terran_Factory.png"));
		
		// load base's health picture
		Health5 = new JLabel(new ImageIcon("Resources\\Images\\Base\\baseHealth1.png"));
		Health4 = new JLabel(new ImageIcon("Resources\\Images\\Base\\baseHealth2.png"));
		Health3 = new JLabel(new ImageIcon("Resources\\Images\\Base\\baseHealth3.png"));
		Health2 = new JLabel(new ImageIcon("Resources\\Images\\Base\\baseHealth4.png"));
		Health1 = new JLabel(new ImageIcon("Resources\\Images\\Base\\baseHealth5.png"));
		HealthDEAD = new JLabel(new ImageIcon("Resources\\Images\\Base\\baseHealthDEAD.png"));

		factory.setBounds(X, Y, 167, 155);

		Health5.setBounds(X-33, Y, 33, 115);
		Health4.setBounds(X-33, Y, 33, 115);
		Health3.setBounds(X-33, Y, 33, 115);
		Health2.setBounds(X-33, Y, 33, 115);
		Health1.setBounds(X-33, Y, 33, 115);
		HealthDEAD.setBounds(X-33, Y, 33, 115);

		factory.setVisible(true);
		
		Health5.setVisible(true);
		Health4.setVisible(false);
		Health3.setVisible(false);
		Health2.setVisible(false);
		Health1.setVisible(false);
		HealthDEAD.setVisible(false);
		
		panel.add(Health5);
		panel.add(Health4);
		panel.add(Health3);
		panel.add(Health2);
		panel.add(Health1);
		panel.add(HealthDEAD);
		
		panel.add(factory);

	}
	
	// this function is us used to set the base to non-visible when:
	// 1) player is not in the sector with the base
	// 2) base is destroyed
	public void hideBase(){
		factory.setVisible(false);
		
		Health5.setVisible(false);
		Health4.setVisible(false);
		Health3.setVisible(false);
		Health2.setVisible(false);
		Health1.setVisible(false);
		HealthDEAD.setVisible(false);
	}
	
	// this function is us used to set the base to visible when:
	// 1) player is in the sector with the base
	public void ShowBase(){
		factory.setVisible(true);
		// by setting them all to false we can display only the picture with the current health
		Health5.setVisible(false);
		Health4.setVisible(false);
		Health3.setVisible(false);
		Health2.setVisible(false);
		Health1.setVisible(false);
		HealthDEAD.setVisible(false);
		
		// display the image with the current health
		if(health == 5) Health5.setVisible(true);
		if(health == 4) Health4.setVisible(true);
		if(health == 3) Health3.setVisible(true);
		if(health == 2) Health2.setVisible(true);
		if(health == 1) Health1.setVisible(true);
		if(health == 0) HealthDEAD.setVisible(true);

	}
	
	// changes the health picture to represent the current health levels
	public void updateHealthImage(){
		if(health == 5){
			Health5.setVisible(true);
			Health4.setVisible(false);
			Health3.setVisible(false);
			Health2.setVisible(false);
			Health1.setVisible(false);
			HealthDEAD.setVisible(false);
		}
		if(health == 4){
			Health5.setVisible(false);
			Health4.setVisible(true);
			Health3.setVisible(false);
			Health2.setVisible(false);
			Health1.setVisible(false);
			HealthDEAD.setVisible(false);
		}
		if(health == 3){
			Health5.setVisible(false);
			Health4.setVisible(false);
			Health3.setVisible(true);
			Health2.setVisible(false);
			Health1.setVisible(false);
			HealthDEAD.setVisible(false);
		}
		if(health == 2){
			Health5.setVisible(false);
			Health4.setVisible(false);
			Health3.setVisible(false);
			Health2.setVisible(true);
			Health1.setVisible(false);
			HealthDEAD.setVisible(false);
		}
		if(health == 1){
			Health5.setVisible(false);
			Health4.setVisible(false);
			Health3.setVisible(false);
			Health2.setVisible(false);
			Health1.setVisible(true);
			HealthDEAD.setVisible(false);
		}
		if(health == 0){
			Health5.setVisible(false);
			Health4.setVisible(false);
			Health3.setVisible(false);
			Health2.setVisible(false);
			Health1.setVisible(false);
			HealthDEAD.setVisible(true);
		}
	}
	
	
}
