package GameComponents;

import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class World {
	
	// background labels
	JLabel jl_Background1;
	JLabel jl_Background2;
	JLabel jl_Background3;
	
	// storage vectors for easy transportation to other classes and functions
	Vector<Object> objectsSector1 = new Vector<Object>();
	Vector<Object> objectsSector2 = new Vector<Object>();
	Vector<Object> objectsSector3 = new Vector<Object>();
	Vector<Vector<Object>> objectCollection = new Vector<Vector<Object>>();
	
	// sector 1
	Object obj1;
	Object obj2;
	Object obj3;
	Object obj4;
	Object obj5;
	Object obj6;
	Object obj7;
	Object obj8;
	Object obj9;
	Object obj10;
	Object obj11;
	Object obj12;
	Object obj13;
	
	// sector 2
	Object obj14;
	Object obj15;
	Object obj16;
	Object obj17;
	Object obj18;
	Object obj19;
	Object obj20;
	Object obj21;
	Object obj22;
	
	// sector 3
	Object obj23;
	Object obj24;
	Object obj25;
	Object obj26;
	Object obj27;
	Object obj28;
	Object obj29;
	Object obj30;
	Object obj31;
	Object obj32;
	Object obj33;
	Object obj34;
	Object obj35;
	
	public World(JPanel panel, Vector<Tank> tanks, int sector){
		
		// load background 1
		jl_Background1 = new JLabel(new ImageIcon("Resources\\Images\\BackGround\\Map2-sector1.png"));
		jl_Background1.setBounds(0, 0, 1610, 920);
		panel.add(jl_Background1);
		jl_Background1.setVisible(false);  // init the background to non-visible.  loadSector functions displays correct background image
		
		// load background 2
		jl_Background2 = new JLabel(new ImageIcon("Resources\\Images\\BackGround\\Map2-sector2.png"));
		jl_Background2.setBounds(0, 0, 1610, 920);
		panel.add(jl_Background2);
		jl_Background2.setVisible(false);  // init the background to non-visible.  loadSector functions displays correct background image
		
		// load background 3
		jl_Background3 = new JLabel(new ImageIcon("Resources\\Images\\BackGround\\Map2-sector3.png"));
		jl_Background3.setBounds(0, 0, 1610, 920);
		panel.add(jl_Background3);
		jl_Background3.setVisible(false);  // init the background to non-visible.  loadSector functions displays correct background image
		
		
		
		//init object collision bounds
		// sector 1
		obj1 = new Object(12, 1608, 94, 2);
		obj2 = new Object(80, 350, 350, 17);
		obj3 = new Object(281, 400, 666, 296);
		obj4 = new Object(0, 30, 923, 0);
		obj5 = new Object(379, 244, 512, 92);
		obj6 = new Object(377, 622, 553, 399);
		obj7 = new Object(545, 507, 690, 345);
		obj8 = new Object(725, 129, 840, 14);
		obj9 = new Object(772, 538, 873, 346);
		obj10 = new Object(826, 1611, 918, 12);
		obj11 = new Object(227, 983, 414, 953);
		obj12 = new Object(574, 983, 761, 953);
		obj13 = new Object(289, 1318, 670, 1288);
		objectsSector1.add(obj1);
		objectsSector1.add(obj2);
		objectsSector1.add(obj3);
		objectsSector1.add(obj4);
		objectsSector1.add(obj5);
		objectsSector1.add(obj6);
		objectsSector1.add(obj7);
		objectsSector1.add(obj8);
		objectsSector1.add(obj9);
		objectsSector1.add(obj10);
		objectsSector1.add(obj11);
		objectsSector1.add(obj12);
		objectsSector1.add(obj13);
		
		// sector 2
		obj14 = new Object(0, 1610, 92, 0);
		obj15 = new Object(20, 494, 270, 294);
		obj16 = new Object(72, 858, 224, 768);
		obj17 = new Object(20, 1328, 270, 1128);
		obj18 = new Object(410, 674, 538, 540);
		obj19 = new Object(472, 536, 574, 446);
		obj20 = new Object(392, 1052, 508, 936);
		obj21 = new Object(508, 1116, 638, 956);
		obj22 = new Object(826, 1612, 918, 0);
		objectsSector2.add(obj14);
		objectsSector2.add(obj15);
		objectsSector2.add(obj16);
		objectsSector2.add(obj17);
		objectsSector2.add(obj18);
		objectsSector2.add(obj19);
		objectsSector2.add(obj20);
		objectsSector2.add(obj21);
		objectsSector2.add(obj22);

		// sector 3
		obj23 = new Object(1, 1599, 93, 3);
		obj24 = new Object(288, 323, 669, 293);
		obj25 = new Object(226, 658, 413, 628);
		obj26 = new Object(573, 658, 760, 628);
		obj27 = new Object(9, 1611, 922, 1581);
		obj28 = new Object(74, 1602, 352, 1262);
		obj29 = new Object(242, 1315, 691, 1211);
		obj30 = new Object(376, 1212, 552, 989);
		obj31 = new Object(540, 1270, 685, 1108);
		obj32 = new Object(382, 1523, 514, 1368);
		obj33 = new Object(724, 1597, 839, 1482);
		obj34 = new Object(767, 1266, 868, 1074);
		obj35 = new Object(824, 1599, 916, 0);
		objectsSector3.add(obj23);
		objectsSector3.add(obj24);
		objectsSector3.add(obj25);
		objectsSector3.add(obj26);
		objectsSector3.add(obj27);
		objectsSector3.add(obj28);
		objectsSector3.add(obj29);
		objectsSector3.add(obj30);
		objectsSector3.add(obj31);
		objectsSector3.add(obj32);
		objectsSector3.add(obj33);
		objectsSector3.add(obj34);
		objectsSector3.add(obj35);
		
		// load object collection vector
		objectCollection.add(objectsSector1);
		objectCollection.add(objectsSector2);
		objectCollection.add(objectsSector3);
		
		

	}
	
	// function name explains what this does...
	public void loadSector1(Vector<Tank> tanks){
		tanks.elementAt(0).currentSector = 1;
		
		// moves tanks
		tanks.elementAt(0).locX += 1500;
		tanks.elementAt(0).moveTankSprites();
		tanks.elementAt(1).locX += 1500;
		tanks.elementAt(1).moveTankSprites();
		
		// moves effects
		tanks.elementAt(0).jl_ShotCloud1.setLocation(tanks.elementAt(0).jl_ShotCloud1.getLocation().x + 1500, tanks.elementAt(0).jl_ShotCloud1.getLocation().y);
		tanks.elementAt(0).jl_ShotCloud2.setLocation(tanks.elementAt(0).jl_ShotCloud2.getLocation().x + 1500, tanks.elementAt(0).jl_ShotCloud2.getLocation().y);
		tanks.elementAt(0).jl_ShotCloud3.setLocation(tanks.elementAt(0).jl_ShotCloud3.getLocation().x + 1500, tanks.elementAt(0).jl_ShotCloud3.getLocation().y);
		tanks.elementAt(1).jl_ShotCloud1.setLocation(tanks.elementAt(1).jl_ShotCloud1.getLocation().x + 1500, tanks.elementAt(1).jl_ShotCloud1.getLocation().y);
		tanks.elementAt(1).jl_ShotCloud2.setLocation(tanks.elementAt(1).jl_ShotCloud2.getLocation().x + 1500, tanks.elementAt(1).jl_ShotCloud2.getLocation().y);
		tanks.elementAt(1).jl_ShotCloud3.setLocation(tanks.elementAt(1).jl_ShotCloud3.getLocation().x + 1500, tanks.elementAt(1).jl_ShotCloud3.getLocation().y);
		
		// moves shells and explosion effects
		for(int i = 0; i < tanks.size(); i++){
			for(int j = 0; j < 10; j++){
				tanks.elementAt(i).shellCollection.elementAt(j).locX += 1500;
				int tempLocX = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().x;
				int tempLocY = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().y;
				tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.setLocation(tempLocX+1500, tempLocY);
				tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud2.setLocation(tempLocX+1500, tempLocY);
				tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud3.setLocation(tempLocX+1500, tempLocY);
			}
		}
	}
	
	// function name explains what this does...
	public void loadSector2(Vector<Tank> tanks, int sector){
		tanks.elementAt(0).currentSector = 2;
		
		if(sector == 1){
			// moves tanks
			tanks.elementAt(0).locX -= 1500;
			tanks.elementAt(0).moveTankSprites();
			tanks.elementAt(1).locX -= 1500;
			tanks.elementAt(1).moveTankSprites();
			
			// moves effects
			tanks.elementAt(0).jl_ShotCloud1.setLocation(tanks.elementAt(0).jl_ShotCloud1.getLocation().x - 1500, tanks.elementAt(0).jl_ShotCloud1.getLocation().y);
			tanks.elementAt(0).jl_ShotCloud2.setLocation(tanks.elementAt(0).jl_ShotCloud2.getLocation().x - 1500, tanks.elementAt(0).jl_ShotCloud2.getLocation().y);
			tanks.elementAt(0).jl_ShotCloud3.setLocation(tanks.elementAt(0).jl_ShotCloud3.getLocation().x - 1500, tanks.elementAt(0).jl_ShotCloud3.getLocation().y);
			tanks.elementAt(1).jl_ShotCloud1.setLocation(tanks.elementAt(1).jl_ShotCloud1.getLocation().x - 1500, tanks.elementAt(1).jl_ShotCloud1.getLocation().y);
			tanks.elementAt(1).jl_ShotCloud2.setLocation(tanks.elementAt(1).jl_ShotCloud2.getLocation().x - 1500, tanks.elementAt(1).jl_ShotCloud2.getLocation().y);
			tanks.elementAt(1).jl_ShotCloud3.setLocation(tanks.elementAt(1).jl_ShotCloud3.getLocation().x - 1500, tanks.elementAt(1).jl_ShotCloud3.getLocation().y);
			
			// moves shells and explosion effects
			for(int i = 0; i < tanks.size(); i++){
				for(int j = 0; j < 10; j++){
					tanks.elementAt(i).shellCollection.elementAt(j).locX -= 1500;
					int tempLocX = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().x;
					int tempLocY = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().y;
					tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.setLocation(tempLocX+1500, tempLocY);
					tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud2.setLocation(tempLocX+1500, tempLocY);
					tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud3.setLocation(tempLocX+1500, tempLocY);
				}
			}
		}
		if(sector == 3){
			// moves tanks
			tanks.elementAt(0).locX += 1500;
			tanks.elementAt(0).moveTankSprites();
			tanks.elementAt(1).locX += 1500;
			tanks.elementAt(1).moveTankSprites();
			
			// moves effects
			tanks.elementAt(0).jl_ShotCloud1.setLocation(tanks.elementAt(0).jl_ShotCloud1.getLocation().x + 1500, tanks.elementAt(0).jl_ShotCloud1.getLocation().y);
			tanks.elementAt(0).jl_ShotCloud2.setLocation(tanks.elementAt(0).jl_ShotCloud2.getLocation().x + 1500, tanks.elementAt(0).jl_ShotCloud2.getLocation().y);
			tanks.elementAt(0).jl_ShotCloud3.setLocation(tanks.elementAt(0).jl_ShotCloud3.getLocation().x + 1500, tanks.elementAt(0).jl_ShotCloud3.getLocation().y);
			tanks.elementAt(1).jl_ShotCloud1.setLocation(tanks.elementAt(1).jl_ShotCloud1.getLocation().x + 1500, tanks.elementAt(1).jl_ShotCloud1.getLocation().y);
			tanks.elementAt(1).jl_ShotCloud2.setLocation(tanks.elementAt(1).jl_ShotCloud2.getLocation().x + 1500, tanks.elementAt(1).jl_ShotCloud2.getLocation().y);
			tanks.elementAt(1).jl_ShotCloud3.setLocation(tanks.elementAt(1).jl_ShotCloud3.getLocation().x + 1500, tanks.elementAt(1).jl_ShotCloud3.getLocation().y);
			
			// moves shells and explosion effects
			for(int i = 0; i < tanks.size(); i++){
				for(int j = 0; j < 10; j++){
					tanks.elementAt(i).shellCollection.elementAt(j).locX += 1500;
					int tempLocX = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().x;
					int tempLocY = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().y;
					tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.setLocation(tempLocX+1500, tempLocY);
					tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud2.setLocation(tempLocX+1500, tempLocY);
					tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud3.setLocation(tempLocX+1500, tempLocY);
				}
			}
		}
	}
	
	// function name explains what this does...
	public void loadSector3(Vector<Tank> tanks){
		tanks.elementAt(0).currentSector = 3;

		// moves tanks
		tanks.elementAt(0).locX -= 1500;
		tanks.elementAt(0).moveTankSprites();
		tanks.elementAt(1).locX -= 1500;
		tanks.elementAt(1).moveTankSprites();
		
		// moves effects
		tanks.elementAt(0).jl_ShotCloud1.setLocation(tanks.elementAt(0).jl_ShotCloud1.getLocation().x - 1500, tanks.elementAt(0).jl_ShotCloud1.getLocation().y);
		tanks.elementAt(0).jl_ShotCloud2.setLocation(tanks.elementAt(0).jl_ShotCloud2.getLocation().x - 1500, tanks.elementAt(0).jl_ShotCloud2.getLocation().y);
		tanks.elementAt(0).jl_ShotCloud3.setLocation(tanks.elementAt(0).jl_ShotCloud3.getLocation().x - 1500, tanks.elementAt(0).jl_ShotCloud3.getLocation().y);
		tanks.elementAt(1).jl_ShotCloud1.setLocation(tanks.elementAt(1).jl_ShotCloud1.getLocation().x - 1500, tanks.elementAt(1).jl_ShotCloud1.getLocation().y);
		tanks.elementAt(1).jl_ShotCloud2.setLocation(tanks.elementAt(1).jl_ShotCloud2.getLocation().x - 1500, tanks.elementAt(1).jl_ShotCloud2.getLocation().y);
		tanks.elementAt(1).jl_ShotCloud3.setLocation(tanks.elementAt(1).jl_ShotCloud3.getLocation().x - 1500, tanks.elementAt(1).jl_ShotCloud3.getLocation().y);
		
		// moves shells and explosion effects
		for(int i = 0; i < tanks.size(); i++){
			for(int j = 0; j < 10; j++){
				tanks.elementAt(i).shellCollection.elementAt(j).locX -= 1500;
				int tempLocX = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().x;
				int tempLocY = tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.getLocation().y;
				tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud1.setLocation(tempLocX+1500, tempLocY);
				tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud2.setLocation(tempLocX+1500, tempLocY);
				tanks.elementAt(i).shellCollection.elementAt(j).jl_HitCloud3.setLocation(tempLocX+1500, tempLocY);
			}
		}
	}
}
