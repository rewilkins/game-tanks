package GameComponents;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


// this game was hard coded for a screen size of 7.5 inches.  roughly 1500 pixels wide.
public class Engine implements KeyListener, MouseListener, MouseMotionListener{
	public static int windowWidth = 600;
	public static int windowHeight = 600;
	JFrame window;
	JPanel panel = new JPanel();
	
	// images for the pause, win and lost screens
	JLabel jl_paused = new JLabel(new ImageIcon("Resources\\Images\\BackGround\\Paused.png"));
	JLabel jl_WIN = new JLabel(new ImageIcon("Resources\\Images\\BackGround\\win.png"));
	JLabel jl_LOOSE = new JLabel(new ImageIcon("Resources\\Images\\BackGround\\lost.png"));
	
	int sector;  // this indicates the current sector that the player is in 
	
	double mouseX;
	double mouseY;
	
	boolean paused = false;
	int tempSpeed = 0;  // used to remember speed when pausing the game
	
	// load the other class files
	World world;
	Base plyrBase;
	Base enmyBase;
	Tank player;
	Tank enemy;
	CollisionDetection collisionDetection = new CollisionDetection();
	
	
	// this vector is passed into other classes.  they are packaged together just to keep the function arguments down
	Vector<Tank> tankCollection = new Vector<Tank>();
	

	// Main
	public static void main(String[] args) {
		new Engine();
	}
	
	
	// Engine's constructor
	public Engine(){
		sector = 1;
		mouseX = 0;
		mouseY = 0;
		
		jl_paused.setBounds(0, 0, 1610, 920);  // init pause JLabel
		jl_WIN.setBounds(0, 0, 1610, 920);  // init win JLabel
		jl_LOOSE.setBounds(0, 0, 1610, 920);  // init Loose JLabel
		panel.add(jl_paused);
		panel.add(jl_WIN);
		panel.add(jl_LOOSE);
		jl_paused.setVisible(false);
		jl_WIN.setVisible(false);
		jl_LOOSE.setVisible(false);
		
		window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// use only one of the following two lines:
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);  // this setting is for a full screen mode
		//window.setBounds(0, 0, windowWidth, windowHeight);  //  this setting is for a pre set widnow size
		
		// NULL allows for us the use a XY-coordinate system plane
		panel.setLayout(null);
		
		
		// make players tank && make enemy tank
		player = new Tank(panel, 100, 600, 1);
		enemy = new Tank(panel, 4350, 600, 3);  // 4350 is the starting possition in the enemy base when the player is in sector 1
		player.initLayerTop(panel);
		enemy.initLayerTop(panel);
		player.initLayerMid(panel);
		enemy.initLayerMid(panel);
		player.initLayerBtm(panel);
		enemy.initLayerBtm(panel);
		tankCollection.add(player);
		tankCollection.add(enemy);
		
		player.speed.setVisible(true);  // this is set to true here and not in the tank constructor because i dont want the enemy tank's label to be visible
		
		// this creates the AI class allowing for an intellegent enemy player
		final AI ai = new AI(tankCollection);
		
		// loads the players bases.  they are seperate from the world class because they have specail properties
		plyrBase = new Base(panel, 82, 365);
		enmyBase = new Base(panel, 1360, 365);
		
		// init world
		world = new World(panel, tankCollection, sector);
		

		//action listeners
		panel.addKeyListener(this);
		panel.addMouseListener(this);
		panel.addMouseMotionListener(this);
		panel.setFocusable(true);

		// init window
		window.add(panel);
		window.setVisible(true);
		
		panel.requestFocusInWindow();  // focus needs to always be in the panel or the action listeners will stop working
		
		// game loop
		final Timer gameLoop = new Timer();
		gameLoop.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				
				// display correct background based on what sector player is in
				if(sector == 1){
					world.jl_Background1.setVisible(true);
					world.jl_Background2.setVisible(false);
					world.jl_Background3.setVisible(false);
					if(plyrBase.health != 0) plyrBase.ShowBase();  // displays the appropriate base for the sector
					if(enmyBase.health != 0) enmyBase.hideBase();  // displays the appropriate base for the sector
				}
				if(sector == 2){
					world.jl_Background1.setVisible(false);
					world.jl_Background2.setVisible(true);
					world.jl_Background3.setVisible(false);
					if(plyrBase.health != 0) plyrBase.hideBase();  // displays the appropriate base for the sector
					if(enmyBase.health != 0) enmyBase.hideBase();  // displays the appropriate base for the sector
				}
				if(sector == 3){
					world.jl_Background1.setVisible(false);
					world.jl_Background2.setVisible(false);
					world.jl_Background3.setVisible(true);
					if(plyrBase.health != 0) plyrBase.hideBase();  // displays the appropriate base for the sector
					if(enmyBase.health != 0) enmyBase.ShowBase();  // displays the appropriate base for the sector
				}
				
				// always moves the HUD to the player
				player.Hud_green_shot.setLocation((int) player.locX, (int) player.locY); 
				player.Hud_yellow_shot.setLocation((int) player.locX, (int) player.locY); 
				player.Hud_red_shot.setLocation((int) player.locX, (int) player.locY); 
				player.Hud_green_NOshot.setLocation((int) player.locX, (int) player.locY); 
				player.Hud_yellow_NOshot.setLocation((int) player.locX, (int) player.locY); 
				player.Hud_red_NOshot.setLocation((int) player.locX, (int) player.locY); 
				player.Hud_dead.setLocation((int) player.locX, (int) player.locY);
				player.displayCurrentHUD();
				player.speed.setText(Integer.toString(player.powerLevel));
				player.speed.setLocation((int) player.locX + 130, (int) player.locY + 80); 
				
				// check if the player is moving to the next screen
				if(sector == 1 && tankCollection.elementAt(0).locX >= 1500){ // 1500 is a hard code dound...  it is the right side of my screen
					world.loadSector2(tankCollection, sector);
					sector = 2; 
				}
				if(sector == 2 && tankCollection.elementAt(0).locX >= 1500){
					world.loadSector3(tankCollection);
					sector = 3;
				}
				if(sector == 3 && tankCollection.elementAt(0).locX <= 10){ // 10 is the left side of my screen
					world.loadSector2(tankCollection, sector);
					sector = 2; 
				}
				if(sector == 2 && tankCollection.elementAt(0).locX <= 10){
					world.loadSector1(tankCollection);
					sector = 1;
				}
				
				// collision detection checks
				collisionDetection.BaseBamage(tankCollection, plyrBase, enmyBase, sector);  // this must come before shell on object collisions!
				collisionDetection.checkTankCollisions(tankCollection);
				collisionDetection.checkShellCollisions(tankCollection);
				collisionDetection.checkTankObjectCollisions(tankCollection, world.objectCollection, sector);
				collisionDetection.checkShellObjectCollision(tankCollection, world.objectCollection, sector);
				
				
				// check respawns
				if(player.alive == false){
					boolean temp = player.respawnTimer(tankCollection.elementAt(0));
					if(temp == true){
						if(sector == 3){
							world.loadSector2(tankCollection, sector); 	// in order for everything to transition correctly,
							world.loadSector1(tankCollection);			// when in sector 3, sector 2 must be loaded then sector 1
							sector = 1;
						}
						if(sector == 2){
							world.loadSector1(tankCollection);
							sector = 1;
						}
						player.respawn(100, 600);
					}
					
				}
				if(enemy.alive == false){
					boolean temp = enemy.respawnTimer(tankCollection.elementAt(1));
					if(temp == true){
						int respawnLocX = 0;
						if(sector == 1) respawnLocX = 4350;
						if(sector == 2) respawnLocX = 2850;
						if(sector == 3) respawnLocX = 1350;
						enemy.respawn(respawnLocX, 600);
					}
				}
				
				// update enemy current sector
				if(player.currentSector == 1){
					if(enemy.locX < 1500) enemy.currentSector = 1;
					if(enemy.locX > 1500 && enemy.locX < 3000) enemy.currentSector = 2;
					if(enemy.locX > 3000 && enemy.locX < 4500) enemy.currentSector = 3;
				}
				if(player.currentSector == 2){
					if(enemy.locX < 0) enemy.currentSector = 1;
					if(enemy.locX > 0 && enemy.locX < 1500) enemy.currentSector = 2;
					if(enemy.locX > 1500 && enemy.locX < 3000) enemy.currentSector = 3;
				}
				if(player.currentSector == 3){
					if(enemy.locX > -3000 && enemy.locX < -1500) enemy.currentSector = 1;
					if(enemy.locX > -1500 && enemy.locX < 0) enemy.currentSector = 2;
					if(enemy.locX > 0) enemy.currentSector = 3;
				}
				
				
				//check for win/loss
				if(enmyBase.health == 0){
					enmyBase.factory.setVisible(false);
					paused = true;
					player.paused = true;
					enemy.paused = true;
					tempSpeed = player.powerLevel;
					player.powerLevel = 0;
					jl_WIN.setVisible(true);
				}else if(plyrBase.health == 0){
					plyrBase.factory.setVisible(false);
					paused = true;
					player.paused = true;
					enemy.paused = true;
					tempSpeed = player.powerLevel;
					player.powerLevel = 0;
					jl_LOOSE.setVisible(true);
				}
				
				
			}
		}, 0, 10);
		
		
	}

	


	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if(player.alive == true){
			switch(keyCode){
			case KeyEvent.VK_W:  // increases speed by one.  max is speed 3
					if(paused == false){
					player.powerLevel += 1;
					if(player.powerLevel == 4) player.powerLevel = 3;  // bumps speed back to one if exceeds max
				}
				break;
				
			case KeyEvent.VK_S: // decreases speed by one.  max reverse speed is -1
				if(paused == false){
					player.powerLevel -= 1;
					if(player.powerLevel == -2) player.powerLevel = -1;  // bumps speed back to -1 if exceeds max reverse speed
				}
				break;
				
			case KeyEvent.VK_A:  // rotates the bottom part of the tank counter-clock wise
				if(paused == false){
					player.rotateLeft();
					panel.requestFocusInWindow();  // focus needs to always be in the panel or the action listeners will stop working
				}
				break;
				
			case KeyEvent.VK_D:  // rotates the bottom part of the tank clockwise
				if(paused == false){
					player.rotateRight();
					panel.requestFocusInWindow();  // focus needs to always be in the panel or the action listeners will stop working
				}
				break;
				
			case KeyEvent.VK_P:  // pauses the game
				// this pauses the game
				if(paused == false){
					paused = true;
					player.paused = true;
					enemy.paused = true;
					tempSpeed = player.powerLevel; // remembers what the players speed was befor game paused
					player.powerLevel = 0;
					jl_paused.setVisible(true);
				}else if(paused == true){
					paused = false;
					player.paused = false;
					enemy.paused = false;
					player.powerLevel = tempSpeed;  // resumes the game and the tank at the speed before game paused
					jl_paused.setVisible(false);
				}
				break;
			
			}
		}
	}
	public void mouseClicked(MouseEvent arg0) {  // shoots the cannon whenever a mouse click happens
		if(player.alive == true && paused == false){
			mouseX = arg0.getX();
			mouseY = arg0.getY();
			player.rotateTurret(mouseX, mouseY);
			player.shoot(mouseX, mouseY);
		}
	}
	public void mouseDragged(MouseEvent arg0) {  		// also shoots the cannon.  when rapid clicking happens mouseDragged 
		if(player.alive == true && paused == false){ 	// gets fire up instead of mouseClick some times so we duplicated the 
			mouseX = arg0.getX();						// click code and put it here
			mouseY = arg0.getY();
			player.rotateTurret(mouseX, mouseY);
			player.shoot(mouseX, mouseY);
		}
	}
	public void mouseMoved(MouseEvent arg0) {  // the turret (upper part) of the tank follows the mouse pointer
		if(player.alive == true && paused == false){
			mouseX = arg0.getX();
			mouseY = arg0.getY();
			player.rotateTurret(mouseX, mouseY);
		}
	}	
		
	// these action listeners are not used
	public void keyReleased(KeyEvent e) {}
	public void keyTyped(KeyEvent e) {}
	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}
	
}
