package GameComponents;


import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Tank {
	
	double locX;
	double locY;
	int startLocX = 0;  // starting location of the tank
	int startLocY = 0;  // starting location of the tank

	int currentSector;
	
	int speedTimerRate = 75;
	
	double angleBottom;
	double angleTop;
	
	int health;
	boolean alive;
	boolean paused;
	
	JLabel respawnTimer;
	int tempTracker = 500; // used for respawning
	
	Vector<Projectile> shellCollection = new Vector<Projectile>();
	Vector<JLabel> effectsCollection = new Vector<JLabel>();
	
	int powerLevel;  // this tells the game what speed the tank is moving.  1-3 forward speeds and 1 backwards speed.  0 = stopped.
	int shellNumber;  // this tells the game what shell (of 10) your tank is shooting.  more info on why this is relevant in following comments.
	
	boolean canShootCannon;
	long cannonFireRate = 1500;  // in milliseconds, this variable is how frequent you can shoot your tank regardless of how many times you click the mouse.
	// cannonFireRate variable should be set with considering Projectile.java's private stopAnimation variable.  the tank should never 
	// reload faster then the stopAnimation variable
	
	
	// creates the 10 needed cannon shells to fire
	private Projectile cannonShell01;
	private Projectile cannonShell02;
	private Projectile cannonShell03; 
	private Projectile cannonShell04;
	private Projectile cannonShell05;
	private Projectile cannonShell06;
	private Projectile cannonShell07;
	private Projectile cannonShell08;
	private Projectile cannonShell09;
	private Projectile cannonShell10;
	
	// creates all the images for the tank.  there are 32 angles so 32 JLables are created.
	// the number in the variable name corresponds to an angle.  EX: jl_Tanktop011_25 = image when at 11.25 degrees (use unit circle as reference).
	// load top of tank
	JLabel jl_Tanktop000_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0024.png"));
	JLabel jl_Tanktop011_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0023.png"));
	JLabel jl_Tanktop022_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0022.png"));
	JLabel jl_Tanktop033_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0021.png"));
	JLabel jl_Tanktop045_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0020.png"));
	JLabel jl_Tanktop056_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0019.png"));
	JLabel jl_Tanktop067_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0018.png"));
	JLabel jl_Tanktop078_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0017.png"));
	JLabel jl_Tanktop090_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0016.png"));

	JLabel jl_Tanktop101_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0015.png"));
	JLabel jl_Tanktop112_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0014.png"));
	JLabel jl_Tanktop123_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0013.png"));
	JLabel jl_Tanktop135_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0012.png"));
	JLabel jl_Tanktop146_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0011.png"));
	JLabel jl_Tanktop157_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0010.png"));
	JLabel jl_Tanktop168_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0009.png"));
	JLabel jl_Tanktop180_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0008.png"));

	JLabel jl_Tanktop191_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0007.png"));
	JLabel jl_Tanktop202_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0006.png"));
	JLabel jl_Tanktop213_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0005.png"));
	JLabel jl_Tanktop225_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0004.png"));
	JLabel jl_Tanktop236_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0003.png"));
	JLabel jl_Tanktop247_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0002.png"));
	JLabel jl_Tanktop258_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0001.png"));
	JLabel jl_Tanktop270_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0000.png"));
	
	JLabel jl_Tanktop281_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0031.png"));
	JLabel jl_Tanktop292_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0030.png"));
	JLabel jl_Tanktop303_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0029.png"));
	JLabel jl_Tanktop315_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0028.png"));
	JLabel jl_Tanktop326_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0027.png"));
	JLabel jl_Tanktop337_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0026.png"));
	JLabel jl_Tanktop348_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\upper part 0025.png"));
	
	
	// load bottom of tank
	JLabel jl_TankBottom000_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0024.png"));
	JLabel jl_TankBottom011_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0023.png"));
	JLabel jl_TankBottom022_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0022.png"));
	JLabel jl_TankBottom033_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0021.png"));
	JLabel jl_TankBottom045_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0020.png"));
	JLabel jl_TankBottom056_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0019.png"));
	JLabel jl_TankBottom067_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0018.png"));
	JLabel jl_TankBottom078_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0017.png"));
	JLabel jl_TankBottom090_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0016.png"));

	JLabel jl_TankBottom101_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0015.png"));
	JLabel jl_TankBottom112_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0014.png"));
	JLabel jl_TankBottom123_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0013.png"));
	JLabel jl_TankBottom135_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0012.png"));
	JLabel jl_TankBottom146_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0011.png"));
	JLabel jl_TankBottom157_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0010.png"));
	JLabel jl_TankBottom168_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0009.png"));
	JLabel jl_TankBottom180_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0008.png"));

	JLabel jl_TankBottom191_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0007.png"));
	JLabel jl_TankBottom202_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0006.png"));
	JLabel jl_TankBottom213_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0005.png"));
	JLabel jl_TankBottom225_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0004.png"));
	JLabel jl_TankBottom236_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0003.png"));
	JLabel jl_TankBottom247_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0002.png"));
	JLabel jl_TankBottom258_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0001.png"));
	JLabel jl_TankBottom270_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0000.png"));
	
	JLabel jl_TankBottom281_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0031.png"));
	JLabel jl_TankBottom292_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0030.png"));
	JLabel jl_TankBottom303_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0029.png"));
	JLabel jl_TankBottom315_00 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0028.png"));
	JLabel jl_TankBottom326_25 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0027.png"));
	JLabel jl_TankBottom337_50 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0026.png"));
	JLabel jl_TankBottom348_75 = new JLabel(new ImageIcon("Resources\\Images\\Tanks\\green tank png\\tank groundpart0025.png"));
	
	
	// load tank effects
	JLabel jl_ShotCloud1 = new JLabel(new ImageIcon("Resources\\Images\\Effects\\Tank Shot Cloud1.png"));
	JLabel jl_ShotCloud2 = new JLabel(new ImageIcon("Resources\\Images\\Effects\\Tank Shot Cloud2.png"));
	JLabel jl_ShotCloud3 = new JLabel(new ImageIcon("Resources\\Images\\Effects\\Tank Shot Cloud3.png"));
	
	// HUD labels
	JLabel Hud_green_shot = new JLabel(new ImageIcon("Resources\\Images\\HUD\\green_fire.png"));
	JLabel Hud_yellow_shot = new JLabel(new ImageIcon("Resources\\Images\\HUD\\yellow_fire.png"));
	JLabel Hud_red_shot = new JLabel(new ImageIcon("Resources\\Images\\HUD\\red_fire.png"));
	JLabel Hud_green_NOshot = new JLabel(new ImageIcon("Resources\\Images\\HUD\\green_NOfire.png"));
	JLabel Hud_yellow_NOshot = new JLabel(new ImageIcon("Resources\\Images\\HUD\\yellow_NOfire.png"));
	JLabel Hud_red_NOshot = new JLabel(new ImageIcon("Resources\\Images\\HUD\\red_NOfire.png"));
	JLabel Hud_dead = new JLabel(new ImageIcon("Resources\\Images\\HUD\\Dead.png"));
	JLabel speed = new JLabel();
	
	
	public Tank(JPanel panel, int startX, int startY, int inputCurrentSector){
		
		currentSector = inputCurrentSector;
		
		// init respawn timer
		respawnTimer = new JLabel("5");
		respawnTimer.setSize(75, 75);
		respawnTimer.setFont (respawnTimer.getFont ().deriveFont (75.0f));
		respawnTimer.setForeground(Color.RED);
		respawnTimer.setLocation(160, 620);
		respawnTimer.setVisible(false);
		panel.add(respawnTimer);
		
		startLocX = startX;
		startLocY = startY;
		
		// arm tank with cannon fire
		cannonShell01 = new Projectile(panel, effectsCollection);
		cannonShell02 = new Projectile(panel, effectsCollection);
		cannonShell03 = new Projectile(panel, effectsCollection);
		cannonShell04 = new Projectile(panel, effectsCollection);
		cannonShell05 = new Projectile(panel, effectsCollection);
		cannonShell06 = new Projectile(panel, effectsCollection);
		cannonShell07 = new Projectile(panel, effectsCollection);
		cannonShell08 = new Projectile(panel, effectsCollection);
		cannonShell09 = new Projectile(panel, effectsCollection);
		cannonShell10 = new Projectile(panel, effectsCollection);
		
		shellCollection.add(cannonShell01);
		shellCollection.add(cannonShell02);
		shellCollection.add(cannonShell03);
		shellCollection.add(cannonShell04);
		shellCollection.add(cannonShell05);
		shellCollection.add(cannonShell06);
		shellCollection.add(cannonShell07);
		shellCollection.add(cannonShell08);
		shellCollection.add(cannonShell09);
		shellCollection.add(cannonShell10);
		
		effectsCollection.add(jl_ShotCloud1);
		effectsCollection.add(jl_ShotCloud2);
		effectsCollection.add(jl_ShotCloud3);

		
		initTank();
		
		angleBottom = 0;  // 0 = facing right
		angleTop = 0;  // 0 = facing right
		//collisionDistance = 0;  // this number varies based of the angle to the other object.  gets set to correct number in CollisionDetection.java
		powerLevel = 0;  // 0 = stopped
		shellNumber = 1;  // 1 = first cannonShell
		canShootCannon = true;  // ready to shoot at start up. (false means must wait reload time before shooting)
		
		health = 3;
		alive = true;
		
		jl_Tanktop000_00.setVisible(true);
		jl_TankBottom000_00.setVisible(true);
		
		
		
		// this timer updates the movement of the tank
		// this timer is in constructor because it must always be running
		final Timer timer_movement = new Timer();
		timer_movement.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {

				if(powerLevel > 0){
					switch(powerLevel){
					case 3:
						moveTank();
					case 2:
						moveTank();
					case 1: 
						moveTank();
					}
				}
				if(powerLevel < 0){
					moveTank();
				}
				//tempPanel.requestFocusInWindow();
			}
		}, 0, speedTimerRate); // (150 = normal) second number will change the simulation speed for how fast the tank moves
		
		
	}
	 
	private void initTank(){
		locX = startLocX;
		locY = startLocY;
		setTopVisibilityFalse();
		setBottomVisibilityFalse();
		jl_ShotCloud1.setVisible(false);
		jl_ShotCloud2.setVisible(false);
		jl_ShotCloud3.setVisible(false);
	}
	public void initLayerTop(JPanel panel){
		// JLabel final initialization and then adding Labels to panel.
		// Effects
		jl_ShotCloud1.setBounds(startLocX, startLocY, 72, 50);
		jl_ShotCloud2.setBounds(startLocX, startLocY, 72, 50);
		jl_ShotCloud3.setBounds(startLocX, startLocY, 72, 50);
		panel.add(jl_ShotCloud1);
		panel.add(jl_ShotCloud2);
		panel.add(jl_ShotCloud3);
		
		//HUD
		Hud_green_shot.setBounds(startLocX, startLocY, 202, 99);
		Hud_yellow_shot.setBounds(startLocX, startLocY, 202, 99);
		Hud_red_shot.setBounds(startLocX, startLocY, 202, 99);
		Hud_green_NOshot.setBounds(startLocX, startLocY, 202, 99);
		Hud_yellow_NOshot.setBounds(startLocX, startLocY, 202, 99);
		Hud_red_NOshot.setBounds(startLocX, startLocY, 202, 99);
		Hud_dead.setBounds(startLocX, startLocY, 202, 99);
		panel.add(Hud_green_shot);
		panel.add(Hud_yellow_shot);
		panel.add(Hud_red_shot);
		panel.add(Hud_green_NOshot);
		panel.add(Hud_yellow_NOshot);
		panel.add(Hud_red_NOshot);
		panel.add(Hud_dead);
		Hud_green_shot.setVisible(false);
		Hud_yellow_shot.setVisible(false);
		Hud_red_shot.setVisible(false);
		Hud_green_NOshot.setVisible(false);
		Hud_yellow_NOshot.setVisible(false);
		Hud_red_NOshot.setVisible(false);
		Hud_dead.setVisible(false);
		
		// HUD speed label
		speed.setForeground(Color.RED);
		speed.setFont(speed.getFont().deriveFont(35.0f));
		speed.setBounds((int) locX + 130, (int) locY + 80, 50, 50);
		speed.setText("0");
		Hud_dead.setVisible(false); 
		panel.add(speed);

	}
	public void initLayerMid(JPanel panel){
		// top of tank
		jl_Tanktop000_00.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop011_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop022_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop033_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop045_00.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop056_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop067_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop078_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop090_00.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_Tanktop000_00);
		panel.add(jl_Tanktop011_25);
		panel.add(jl_Tanktop022_50);
		panel.add(jl_Tanktop033_75);
		panel.add(jl_Tanktop045_00);
		panel.add(jl_Tanktop056_25);
		panel.add(jl_Tanktop067_50);
		panel.add(jl_Tanktop078_75);
		panel.add(jl_Tanktop090_00);

		jl_Tanktop101_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop112_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop123_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop135_00.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop146_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop157_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop168_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop180_00.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_Tanktop101_25);
		panel.add(jl_Tanktop112_50);
		panel.add(jl_Tanktop123_75);
		panel.add(jl_Tanktop135_00);
		panel.add(jl_Tanktop146_25);
		panel.add(jl_Tanktop157_50);
		panel.add(jl_Tanktop168_75);
		panel.add(jl_Tanktop180_00);

		jl_Tanktop191_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop202_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop213_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop225_00.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop236_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop247_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop258_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop270_00.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_Tanktop191_25);
		panel.add(jl_Tanktop202_50);
		panel.add(jl_Tanktop213_75);
		panel.add(jl_Tanktop225_00);
		panel.add(jl_Tanktop236_25);
		panel.add(jl_Tanktop247_50);
		panel.add(jl_Tanktop258_75);
		panel.add(jl_Tanktop270_00);
		
		jl_Tanktop281_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop292_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop303_75.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop315_00.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop326_25.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop337_50.setBounds(startLocX, startLocY, 160, 160);
		jl_Tanktop348_75.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_Tanktop281_25);
		panel.add(jl_Tanktop292_50);
		panel.add(jl_Tanktop303_75);
		panel.add(jl_Tanktop315_00);
		panel.add(jl_Tanktop326_25);
		panel.add(jl_Tanktop337_50);
		panel.add(jl_Tanktop348_75);
	}
	public void initLayerBtm(JPanel panel){
		// bottom of tank
		jl_TankBottom000_00.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom011_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom022_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom033_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom045_00.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom056_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom067_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom078_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom090_00.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_TankBottom000_00);
		panel.add(jl_TankBottom011_25);
		panel.add(jl_TankBottom022_50);
		panel.add(jl_TankBottom033_75);
		panel.add(jl_TankBottom045_00);
		panel.add(jl_TankBottom056_25);
		panel.add(jl_TankBottom067_50);
		panel.add(jl_TankBottom078_75);
		panel.add(jl_TankBottom090_00);

		jl_TankBottom101_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom112_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom123_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom135_00.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom146_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom157_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom168_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom180_00.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_TankBottom101_25);
		panel.add(jl_TankBottom112_50);
		panel.add(jl_TankBottom123_75);
		panel.add(jl_TankBottom135_00);
		panel.add(jl_TankBottom146_25);
		panel.add(jl_TankBottom157_50);
		panel.add(jl_TankBottom168_75);
		panel.add(jl_TankBottom180_00);

		jl_TankBottom191_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom202_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom213_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom225_00.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom236_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom247_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom258_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom270_00.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_TankBottom191_25);
		panel.add(jl_TankBottom202_50);
		panel.add(jl_TankBottom213_75);
		panel.add(jl_TankBottom225_00);
		panel.add(jl_TankBottom236_25);
		panel.add(jl_TankBottom247_50);
		panel.add(jl_TankBottom258_75);
		panel.add(jl_TankBottom270_00);
		
		jl_TankBottom281_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom292_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom303_75.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom315_00.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom326_25.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom337_50.setBounds(startLocX, startLocY, 160, 160);
		jl_TankBottom348_75.setBounds(startLocX, startLocY, 160, 160);
		panel.add(jl_TankBottom281_25);
		panel.add(jl_TankBottom292_50);
		panel.add(jl_TankBottom303_75);
		panel.add(jl_TankBottom315_00);
		panel.add(jl_TankBottom326_25);
		panel.add(jl_TankBottom337_50);
		panel.add(jl_TankBottom348_75);
	}
	
	private void moveTank(){
		
		// the numbers used to move the tank are the least possible
		// to prevent the tank from appearing to be moving sideways
		// these numbers need to be small also to not allow the tank to move too fast
		if(angleBottom == 0.0){
			if(powerLevel > 0) locX += 6;
			if(powerLevel > 0) locY -= 0;
			if(powerLevel < 0) locX -= 6;
			if(powerLevel < 0) locY += 0;
		}
		if(angleBottom == 11.25){
			if(powerLevel > 0) locX += 6;
			if(powerLevel > 0) locY -= 1;
			if(powerLevel < 0) locX -= 6;
			if(powerLevel < 0) locY += 1;
		}
		if(angleBottom == 22.5){
			if(powerLevel > 0) locX += 5;
			if(powerLevel > 0) locY -= 2;
			if(powerLevel < 0) locX -= 5;
			if(powerLevel < 0) locY += 2;
		}
		if(angleBottom == 33.75){
			if(powerLevel > 0) locX += 5;
			if(powerLevel > 0) locY -= 3;
			if(powerLevel < 0) locX -= 5;
			if(powerLevel < 0) locY += 3;
		}
		if(angleBottom == 45.0){
			if(powerLevel > 0) locX += 4;
			if(powerLevel > 0) locY -= 4;
			if(powerLevel < 0) locX -= 4;
			if(powerLevel < 0) locY += 4;
		}
		if(angleBottom == 56.25){
			if(powerLevel > 0) locX += 3;
			if(powerLevel > 0) locY -= 4;
			if(powerLevel < 0) locX -= 3;
			if(powerLevel < 0) locY += 4;
		}
		if(angleBottom == 67.5){
			if(powerLevel > 0) locX += 2;
			if(powerLevel > 0) locY -= 5;
			if(powerLevel < 0) locX -= 2;
			if(powerLevel < 0) locY += 5;
		}
		if(angleBottom == 78.75){
			if(powerLevel > 0) locX += 1;
			if(powerLevel > 0) locY -= 5;
			if(powerLevel < 0) locX -= 1;
			if(powerLevel < 0) locY += 5;
		}
		if(angleBottom == 90.0){
			if(powerLevel > 0) locX += 0;
			if(powerLevel > 0) locY -= 5;
			if(powerLevel < 0) locX -= 0;
			if(powerLevel < 0) locY += 5;
		}
		if(angleBottom == 101.25){
			if(powerLevel > 0) locX -= 1;
			if(powerLevel > 0) locY -= 5;
			if(powerLevel < 0) locX += 1;
			if(powerLevel < 0) locY += 5;
		}
		if(angleBottom == 112.5){
			if(powerLevel > 0) locX -= 2;
			if(powerLevel > 0) locY -= 5;
			if(powerLevel < 0) locX += 2;
			if(powerLevel < 0) locY += 5;
		}
		if(angleBottom == 123.75){
			if(powerLevel > 0) locX -= 3;
			if(powerLevel > 0) locY -= 4;
			if(powerLevel < 0) locX += 3;
			if(powerLevel < 0) locY += 4;
		}
		if(angleBottom == 135.0){
			if(powerLevel > 0) locX -= 4;
			if(powerLevel > 0) locY -= 4;
			if(powerLevel < 0) locX += 4;
			if(powerLevel < 0) locY += 4;
		}
		if(angleBottom == 146.25){
			if(powerLevel > 0) locX -= 5;
			if(powerLevel > 0) locY -= 3;
			if(powerLevel < 0) locX += 5;
			if(powerLevel < 0) locY += 3;
		}
		if(angleBottom == 157.5){
			if(powerLevel > 0) locX -= 5;
			if(powerLevel > 0) locY -= 2;
			if(powerLevel < 0) locX += 5;
			if(powerLevel < 0) locY += 2;
		}
		if(angleBottom == 168.75){
			if(powerLevel > 0) locX -= 6;
			if(powerLevel > 0) locY -= 1;
			if(powerLevel < 0) locX += 6;
			if(powerLevel < 0) locY += 1;
		}
		if(angleBottom == 180.0){
			if(powerLevel > 0) locX -= 6;
			if(powerLevel > 0) locY -= 0;
			if(powerLevel < 0) locX += 6;
			if(powerLevel < 0) locY += 0;
		}
		if(angleBottom == 191.25){
			if(powerLevel > 0) locX -= 6;
			if(powerLevel > 0) locY += 1;
			if(powerLevel < 0) locX += 6;
			if(powerLevel < 0) locY -= 1;
		}
		if(angleBottom == 202.5){
			if(powerLevel > 0) locX -= 5;
			if(powerLevel > 0) locY += 2;
			if(powerLevel < 0) locX += 5;
			if(powerLevel < 0) locY -= 2;
		}
		if(angleBottom == 213.75){
			if(powerLevel > 0) locX -= 5;
			if(powerLevel > 0) locY += 3;
			if(powerLevel < 0) locX += 5;
			if(powerLevel < 0) locY -= 3;
		}
		if(angleBottom == 225.0){
			if(powerLevel > 0) locX -= 4;
			if(powerLevel > 0) locY += 4;
			if(powerLevel < 0) locX += 4;
			if(powerLevel < 0) locY -= 4;
		}
		if(angleBottom == 236.25){
			if(powerLevel > 0) locX -= 3;
			if(powerLevel > 0) locY += 4;
			if(powerLevel < 0) locX += 3;
			if(powerLevel < 0) locY -= 4;
		}
		if(angleBottom == 247.5){
			if(powerLevel > 0) locX -= 2;
			if(powerLevel > 0) locY += 5;
			if(powerLevel < 0) locX += 2;
			if(powerLevel < 0) locY -= 5;
		}
		if(angleBottom == 258.75){
			if(powerLevel > 0) locX -= 1;
			if(powerLevel > 0) locY += 5;
			if(powerLevel < 0) locX += 1;
			if(powerLevel < 0) locY -= 5;
		}
		if(angleBottom == 270.0){
			if(powerLevel > 0) locX -= 0;
			if(powerLevel > 0) locY += 5;
			if(powerLevel < 0) locX += 0;
			if(powerLevel < 0) locY -= 5;
		}
		if(angleBottom == 281.25){
			if(powerLevel > 0) locX += 1;
			if(powerLevel > 0) locY += 5;
			if(powerLevel < 0) locX -= 1;
			if(powerLevel < 0) locY -= 5;
		}
		if(angleBottom == 292.5){
			if(powerLevel > 0) locX += 2;
			if(powerLevel > 0) locY += 5;
			if(powerLevel < 0) locX -= 2;
			if(powerLevel < 0) locY -= 5;
		}
		if(angleBottom == 303.75){
			if(powerLevel > 0) locX += 3;
			if(powerLevel > 0) locY += 4;
			if(powerLevel < 0) locX -= 3;
			if(powerLevel < 0) locY -= 4;
		}
		if(angleBottom == 315.0){
			if(powerLevel > 0) locX += 4;
			if(powerLevel > 0) locY += 4;
			if(powerLevel < 0) locX -= 4;
			if(powerLevel < 0) locY -= 4;
		}
		if(angleBottom == 326.25){
			if(powerLevel > 0) locX += 5;
			if(powerLevel > 0) locY += 3;
			if(powerLevel < 0) locX -= 5;
			if(powerLevel < 0) locY -= 3;
		}
		if(angleBottom == 337.5){
			if(powerLevel > 0) locX += 5;
			if(powerLevel > 0) locY += 2;
			if(powerLevel < 0) locX -= 5;
			if(powerLevel < 0) locY -= 2;
		}
		if(angleBottom == 348.75){
			if(powerLevel > 0) locX += 6;
			if(powerLevel > 0) locY += 1;
			if(powerLevel < 0) locX -= 6;
			if(powerLevel < 0) locY -= 1;
		}
		
		moveTankSprites();
		
	}
	// this relocates all the images to the location of the tank
	public void moveTankSprites(){
		jl_Tanktop000_00.setLocation((int)locX, (int)locY);
		jl_Tanktop011_25.setLocation((int)locX, (int)locY);
		jl_Tanktop022_50.setLocation((int)locX, (int)locY);
		jl_Tanktop033_75.setLocation((int)locX, (int)locY);
		jl_Tanktop045_00.setLocation((int)locX, (int)locY);
		jl_Tanktop056_25.setLocation((int)locX, (int)locY);
		jl_Tanktop067_50.setLocation((int)locX, (int)locY);
		jl_Tanktop078_75.setLocation((int)locX, (int)locY);
		jl_Tanktop090_00.setLocation((int)locX, (int)locY);

		jl_Tanktop101_25.setLocation((int)locX, (int)locY);
		jl_Tanktop112_50.setLocation((int)locX, (int)locY);
		jl_Tanktop123_75.setLocation((int)locX, (int)locY);
		jl_Tanktop135_00.setLocation((int)locX, (int)locY);
		jl_Tanktop146_25.setLocation((int)locX, (int)locY);
		jl_Tanktop157_50.setLocation((int)locX, (int)locY);
		jl_Tanktop168_75.setLocation((int)locX, (int)locY);
		jl_Tanktop180_00.setLocation((int)locX, (int)locY);

		jl_Tanktop191_25.setLocation((int)locX, (int)locY);
		jl_Tanktop202_50.setLocation((int)locX, (int)locY);
		jl_Tanktop213_75.setLocation((int)locX, (int)locY);
		jl_Tanktop225_00.setLocation((int)locX, (int)locY);
		jl_Tanktop236_25.setLocation((int)locX, (int)locY);
		jl_Tanktop247_50.setLocation((int)locX, (int)locY);
		jl_Tanktop258_75.setLocation((int)locX, (int)locY);
		jl_Tanktop270_00.setLocation((int)locX, (int)locY);
		
		jl_Tanktop281_25.setLocation((int)locX, (int)locY);
		jl_Tanktop292_50.setLocation((int)locX, (int)locY);
		jl_Tanktop303_75.setLocation((int)locX, (int)locY);
		jl_Tanktop315_00.setLocation((int)locX, (int)locY);
		jl_Tanktop326_25.setLocation((int)locX, (int)locY);
		jl_Tanktop337_50.setLocation((int)locX, (int)locY);
		jl_Tanktop348_75.setLocation((int)locX, (int)locY);
		
		
		
		
		jl_TankBottom000_00.setLocation((int)locX, (int)locY);
		jl_TankBottom011_25.setLocation((int)locX, (int)locY);
		jl_TankBottom022_50.setLocation((int)locX, (int)locY);
		jl_TankBottom033_75.setLocation((int)locX, (int)locY);
		jl_TankBottom045_00.setLocation((int)locX, (int)locY);
		jl_TankBottom056_25.setLocation((int)locX, (int)locY);
		jl_TankBottom067_50.setLocation((int)locX, (int)locY);
		jl_TankBottom078_75.setLocation((int)locX, (int)locY);
		jl_TankBottom090_00.setLocation((int)locX, (int)locY);

		jl_TankBottom101_25.setLocation((int)locX, (int)locY);
		jl_TankBottom112_50.setLocation((int)locX, (int)locY);
		jl_TankBottom123_75.setLocation((int)locX, (int)locY);
		jl_TankBottom135_00.setLocation((int)locX, (int)locY);
		jl_TankBottom146_25.setLocation((int)locX, (int)locY);
		jl_TankBottom157_50.setLocation((int)locX, (int)locY);
		jl_TankBottom168_75.setLocation((int)locX, (int)locY);
		jl_TankBottom180_00.setLocation((int)locX, (int)locY);

		jl_TankBottom191_25.setLocation((int)locX, (int)locY);;
		jl_TankBottom202_50.setLocation((int)locX, (int)locY);
		jl_TankBottom213_75.setLocation((int)locX, (int)locY);
		jl_TankBottom225_00.setLocation((int)locX, (int)locY);
		jl_TankBottom236_25.setLocation((int)locX, (int)locY);
		jl_TankBottom247_50.setLocation((int)locX, (int)locY);
		jl_TankBottom258_75.setLocation((int)locX, (int)locY);
		jl_TankBottom270_00.setLocation((int)locX, (int)locY);
		
		jl_TankBottom281_25.setLocation((int)locX, (int)locY);
		jl_TankBottom292_50.setLocation((int)locX, (int)locY);
		jl_TankBottom303_75.setLocation((int)locX, (int)locY);
		jl_TankBottom315_00.setLocation((int)locX, (int)locY);
		jl_TankBottom326_25.setLocation((int)locX, (int)locY);
		jl_TankBottom337_50.setLocation((int)locX, (int)locY);
		jl_TankBottom348_75.setLocation((int)locX, (int)locY);
	}

	public void displayCurrentHUD(){

		if(health == 3 && canShootCannon == true){
			Hud_green_shot.setVisible(true);
			Hud_yellow_shot.setVisible(false);
			Hud_red_shot.setVisible(false);
			Hud_green_NOshot.setVisible(false);
			Hud_yellow_NOshot.setVisible(false);
			Hud_red_NOshot.setVisible(false);
			Hud_dead.setVisible(false);
		}
		if(health == 2 && canShootCannon == true){
			Hud_green_shot.setVisible(false);
			Hud_yellow_shot.setVisible(true);
			Hud_red_shot.setVisible(false);
			Hud_green_NOshot.setVisible(false);
			Hud_yellow_NOshot.setVisible(false);
			Hud_red_NOshot.setVisible(false);
			Hud_dead.setVisible(false);
		}
		if(health == 1 && canShootCannon == true){
			Hud_green_shot.setVisible(false);
			Hud_yellow_shot.setVisible(false);
			Hud_red_shot.setVisible(true);
			Hud_green_NOshot.setVisible(false);
			Hud_yellow_NOshot.setVisible(false);
			Hud_red_NOshot.setVisible(false);
			Hud_dead.setVisible(false);
		}
		if(health == 3 && canShootCannon == false){
			Hud_green_shot.setVisible(false);
			Hud_yellow_shot.setVisible(false);
			Hud_red_shot.setVisible(false);
			Hud_green_NOshot.setVisible(true);
			Hud_yellow_NOshot.setVisible(false);
			Hud_red_NOshot.setVisible(false);
			Hud_dead.setVisible(false);
		}
		if(health == 2 && canShootCannon == false){
			Hud_green_shot.setVisible(false);
			Hud_yellow_shot.setVisible(false);
			Hud_red_shot.setVisible(false);
			Hud_green_NOshot.setVisible(false);
			Hud_yellow_NOshot.setVisible(true);
			Hud_red_NOshot.setVisible(false);
			Hud_dead.setVisible(false);
		}
		if(health == 1 && canShootCannon == false){
			Hud_green_shot.setVisible(false);
			Hud_yellow_shot.setVisible(false);
			Hud_red_shot.setVisible(false);
			Hud_green_NOshot.setVisible(false);
			Hud_yellow_NOshot.setVisible(false);
			Hud_red_NOshot.setVisible(true);
			Hud_dead.setVisible(false);
		}
		if(health == 0){
			Hud_green_shot.setVisible(false);
			Hud_yellow_shot.setVisible(false);
			Hud_red_shot.setVisible(false);
			Hud_green_NOshot.setVisible(false);
			Hud_yellow_NOshot.setVisible(false);
			Hud_red_NOshot.setVisible(false);
			Hud_dead.setVisible(true);
		}
	}
	
	public void rotateRight(){
		angleBottom -= 11.25;
		if(angleBottom == -11.25) angleBottom = 348.75;
		findBottomImage(angleBottom);
		
	}
	public void rotateLeft(){
		angleBottom += 11.25;
		if(angleBottom == 360) angleBottom = 0;
		findBottomImage(angleBottom);
		
	}
	public void rotateTurret(double x, double y){
		double tankX = locX + 80;
		double tankY = locY + 58;

		double adj = 0;  // Adjacent edge
		double hyp = 0;  // Hypotenuse
		
		// calc triangle to get angle from tank to mouse
		adj = Math.sqrt((Math.pow((tankX - x),2)) + (Math.pow((0),2)));
		hyp = Math.sqrt((Math.pow((tankX - x),2)) + (Math.pow((tankY - y),2)));
		// calc angle from triangle
		double angle = Math.acos(adj / hyp);
		angle = Math.toDegrees(angle);  // puts rads into degrees
		
		// mod angle result to get appropriate angle in other quadrants
		if(x >= tankX && y < tankY){}  // Quadrant 1 - no mods
		if(x < tankX && y <= tankY) angle = (90 - (angle - 90));  // Quadrant 2
		if(x <= tankX && y > tankY) angle = (180 + angle);  // Quadrant 3
		if(x > tankX && y >= tankY) angle = (270 - (angle - 90));  // Quadrant 4
		
		findTopImage(angle);
	}
	
	
	public void setBottomVisibilityFalse(){
		jl_TankBottom000_00.setVisible(false);
		jl_TankBottom011_25.setVisible(false);
		jl_TankBottom022_50.setVisible(false);
		jl_TankBottom033_75.setVisible(false);
		jl_TankBottom045_00.setVisible(false);
		jl_TankBottom056_25.setVisible(false);
		jl_TankBottom067_50.setVisible(false);
		jl_TankBottom078_75.setVisible(false);
		jl_TankBottom090_00.setVisible(false);
		jl_TankBottom101_25.setVisible(false);
		jl_TankBottom112_50.setVisible(false);
		jl_TankBottom123_75.setVisible(false);
		jl_TankBottom135_00.setVisible(false);
		jl_TankBottom146_25.setVisible(false);
		jl_TankBottom157_50.setVisible(false);
		jl_TankBottom168_75.setVisible(false);
		jl_TankBottom180_00.setVisible(false);
		jl_TankBottom191_25.setVisible(false);
		jl_TankBottom202_50.setVisible(false);
		jl_TankBottom213_75.setVisible(false);
		jl_TankBottom225_00.setVisible(false);
		jl_TankBottom236_25.setVisible(false);
		jl_TankBottom247_50.setVisible(false);
		jl_TankBottom258_75.setVisible(false);
		jl_TankBottom270_00.setVisible(false);
		jl_TankBottom281_25.setVisible(false);
		jl_TankBottom292_50.setVisible(false);
		jl_TankBottom303_75.setVisible(false);
		jl_TankBottom315_00.setVisible(false);
		jl_TankBottom326_25.setVisible(false);
		jl_TankBottom337_50.setVisible(false);
		jl_TankBottom348_75.setVisible(false);
	}
	// finds the active bottom of the tank image
	private void findBottomImage(double angle){
		
		setBottomVisibilityFalse();
		
		if(angle == 0.0){
			jl_TankBottom000_00.setVisible(true);
		}
		if(angle == 11.25){
			jl_TankBottom011_25.setVisible(true);
		}
		if(angle == 22.5){
			jl_TankBottom022_50.setVisible(true);
		}
		if(angle == 33.75){
			jl_TankBottom033_75.setVisible(true);
		}
		if(angle == 45.0){
			jl_TankBottom045_00.setVisible(true);
		}
		if(angle == 56.25){
			jl_TankBottom056_25.setVisible(true);
		}
		if(angle == 67.5){
			jl_TankBottom067_50.setVisible(true);
		}
		if(angle == 78.75){
			jl_TankBottom078_75.setVisible(true);
		}
		if(angle == 90.0){
			jl_TankBottom090_00.setVisible(true);
		}
		if(angle == 101.25){
			jl_TankBottom101_25.setVisible(true);
		}
		if(angle == 112.5){
			jl_TankBottom112_50.setVisible(true);
		}
		if(angle == 123.75){
			jl_TankBottom123_75.setVisible(true);
		}
		if(angle == 135.0){
			jl_TankBottom135_00.setVisible(true);
		}
		if(angle == 146.25){
			jl_TankBottom146_25.setVisible(true);
		}
		if(angle == 157.5){
			jl_TankBottom157_50.setVisible(true);
		}
		if(angle == 168.75){
			jl_TankBottom168_75.setVisible(true);
		}
		if(angle == 180.0){
			jl_TankBottom180_00.setVisible(true);
		}
		if(angle == 191.25){
			jl_TankBottom191_25.setVisible(true);
		}
		if(angle == 202.5){
			jl_TankBottom202_50.setVisible(true);
		}
		if(angle == 213.75){
			jl_TankBottom213_75.setVisible(true);
		}
		if(angle == 225.0){
			jl_TankBottom225_00.setVisible(true);
		}
		if(angle == 236.25){
			jl_TankBottom236_25.setVisible(true);
		}
		if(angle == 247.5){
			jl_TankBottom247_50.setVisible(true);
		}
		if(angle == 258.75){
			jl_TankBottom258_75.setVisible(true);
		}
		if(angle == 270.0){
			jl_TankBottom270_00.setVisible(true);
		}
		if(angle == 281.25){
			jl_TankBottom281_25.setVisible(true);
		}
		if(angle == 292.5){
			jl_TankBottom292_50.setVisible(true);
		}
		if(angle == 303.75){
			jl_TankBottom303_75.setVisible(true);
		}
		if(angle == 315.0){
			jl_TankBottom315_00.setVisible(true);
		}
		if(angle == 326.25){
			jl_TankBottom326_25.setVisible(true);
		}
		if(angle == 337.5){
			jl_TankBottom337_50.setVisible(true);
		}
		if(angle == 348.75){
			jl_TankBottom348_75.setVisible(true);
		}
	}
	
	
	public void setTopVisibilityFalse(){
		jl_Tanktop000_00.setVisible(false);
		jl_Tanktop011_25.setVisible(false);
		jl_Tanktop022_50.setVisible(false);
		jl_Tanktop033_75.setVisible(false);
		jl_Tanktop045_00.setVisible(false);
		jl_Tanktop056_25.setVisible(false);
		jl_Tanktop067_50.setVisible(false);
		jl_Tanktop078_75.setVisible(false);
		jl_Tanktop090_00.setVisible(false);
		jl_Tanktop101_25.setVisible(false);
		jl_Tanktop112_50.setVisible(false);
		jl_Tanktop123_75.setVisible(false);
		jl_Tanktop135_00.setVisible(false);
		jl_Tanktop146_25.setVisible(false);
		jl_Tanktop157_50.setVisible(false);
		jl_Tanktop168_75.setVisible(false);
		jl_Tanktop180_00.setVisible(false);
		jl_Tanktop191_25.setVisible(false);
		jl_Tanktop202_50.setVisible(false);
		jl_Tanktop213_75.setVisible(false);
		jl_Tanktop225_00.setVisible(false);
		jl_Tanktop236_25.setVisible(false);
		jl_Tanktop247_50.setVisible(false);
		jl_Tanktop258_75.setVisible(false);
		jl_Tanktop270_00.setVisible(false);
		jl_Tanktop281_25.setVisible(false);
		jl_Tanktop292_50.setVisible(false);
		jl_Tanktop303_75.setVisible(false);
		jl_Tanktop315_00.setVisible(false);
		jl_Tanktop326_25.setVisible(false);
		jl_Tanktop337_50.setVisible(false);
		jl_Tanktop348_75.setVisible(false);
	}
	// finds the active top of the tank image
	private void findTopImage(double angle){
			
		setTopVisibilityFalse();

		if((angle >= 354.375 && angle <= 360) || (angle >= 0.0 && angle < 5.625)){
			angleTop = 0.0;
			jl_Tanktop000_00.setVisible(true);
		}
		if(angle >= 5.625 && angle < 16.875){
			angleTop = 11.25;
			jl_Tanktop011_25.setVisible(true);
		}
		if(angle >= 16.875 && angle < 28.125){
			angleTop = 22.5;
			jl_Tanktop022_50.setVisible(true);
		}
		if(angle >= 28.125 && angle < 39.375){
			angleTop = 33.75;
			jl_Tanktop033_75.setVisible(true);
		}
		if(angle >= 39.375 && angle < 50.625){
			angleTop = 45.0;
			jl_Tanktop045_00.setVisible(true);
		}
		if(angle >= 50.625 && angle < 61.875){
			angleTop = 56.25;
			jl_Tanktop056_25.setVisible(true);
		}
		if(angle >= 61.875 && angle < 73.125){
			angleTop = 67.5;
			jl_Tanktop067_50.setVisible(true);
		}
		if(angle >= 73.125 && angle < 84.375){
			angleTop = 78.75;
			jl_Tanktop078_75.setVisible(true);
		}
		if(angle >= 84.375 && angle < 95.625){
			angleTop = 90.0;
			jl_Tanktop090_00.setVisible(true);
		}
		if(angle >= 95.625 && angle < 106.875){
			angleTop = 101.25;
			jl_Tanktop101_25.setVisible(true);
		}
		if(angle >= 106.875 && angle < 118.125){
			angleTop = 112.5;
			jl_Tanktop112_50.setVisible(true);
		}
		if(angle >= 118.125 && angle < 129.375){
			angleTop = 123.75;
			jl_Tanktop123_75.setVisible(true);
		}
		if(angle >= 129.375 && angle < 140.625){
			angleTop = 135.0;
			jl_Tanktop135_00.setVisible(true);
		}
		if(angle >= 140.625 && angle < 151.875){
			angleTop = 146.25;
			jl_Tanktop146_25.setVisible(true);
		}
		if(angle >= 151.875 && angle < 163.125){
			angleTop = 157.5;
			jl_Tanktop157_50.setVisible(true);
		}
		if(angle >= 163.125 && angle < 174.375){
			angleTop = 168.75;
			jl_Tanktop168_75.setVisible(true);
		}
		if(angle >= 174.375 && angle < 185.625){
			angleTop = 180.0;
			jl_Tanktop180_00.setVisible(true);
		}
		if(angle >= 185.625 && angle < 196.875){
			angleTop = 191.25;
			jl_Tanktop191_25.setVisible(true);
		}
		if(angle >= 196.875 && angle < 208.125){
			angleTop = 202.5;
			jl_Tanktop202_50.setVisible(true);
		}
		if(angle >= 208.125 && angle < 219.375){
			angleTop = 213.75;
			jl_Tanktop213_75.setVisible(true);
		}
		if(angle >= 219.375 && angle < 230.625){
			angleTop = 225.0;
			jl_Tanktop225_00.setVisible(true);
		}
		if(angle >= 230.625 && angle < 241.875){
			angleTop = 236.25;
			jl_Tanktop236_25.setVisible(true);
		}
		if(angle >= 241.875 && angle < 253.125){
			angleTop = 247.5;
			jl_Tanktop247_50.setVisible(true);
		}
		if(angle >= 253.125 && angle < 264.375){
			angleTop = 258.75;
			jl_Tanktop258_75.setVisible(true);
		}
		if(angle >= 264.375 && angle < 275.625){
			angleTop = 270.0;
			jl_Tanktop270_00.setVisible(true);
		}
		if(angle >= 275.625 && angle < 286.875){
			angleTop = 281.25;
			jl_Tanktop281_25.setVisible(true);
		}
		if(angle >= 286.875 && angle < 298.125){
			angleTop = 292.5;
			jl_Tanktop292_50.setVisible(true);
		}
		if(angle >= 298.125 && angle < 309.375){
			angleTop = 303.75;
			jl_Tanktop303_75.setVisible(true);
		}
		if(angle >= 309.375 && angle < 320.625){
			angleTop = 315.0;
			jl_Tanktop315_00.setVisible(true);
		}
		if(angle >= 320.625 && angle < 331.875){
			angleTop = 326.25;
			jl_Tanktop326_25.setVisible(true);
		}
		if(angle >= 331.875 && angle < 343.125){
			angleTop = 337.5;
			jl_Tanktop337_50.setVisible(true);
		}
		if(angle >= 343.125 && angle < 354.375){
			angleTop = 348.75;
			jl_Tanktop348_75.setVisible(true);
		}

	}
	
	
	public void checkHealth(){
		
		health--;
		if(health == 0){
			die();
			
		}
	}
	private void die(){
		paused = true;
		alive = false;
		setTopVisibilityFalse();
		setBottomVisibilityFalse();
		powerLevel = 0;

	}
	
	// this timer ticks down 5 seconds then fires the respawn functions
	public boolean respawnTimer(Tank tank){
		respawnTimer.setLocation((int) tank.locX-5, (int) tank.locY);
		respawnTimer.setVisible(true);

		if(tempTracker == 500) respawnTimer.setText("5");
		if(tempTracker == 400) respawnTimer.setText("4");
		if(tempTracker == 300) respawnTimer.setText("3");
		if(tempTracker == 200) respawnTimer.setText("2");
		if(tempTracker == 100) respawnTimer.setText("1");
		if(tempTracker == 000) respawnTimer.setText("0");
		if(tempTracker == -100){
			tempTracker = 500;
			respawnTimer.setVisible(false);
			respawnTimer.setText("5");
			return true;
		}
		tempTracker--;
		return false;
	}
	public void respawn(int X, int Y){
		
		paused = false;
		health = 3;
		alive = true;

		powerLevel = 0;
		locX = X;
		locY = Y;
		moveTankSprites();
		
		shellNumber = 1;  // 1 = first cannonShell
		canShootCannon = true;  // ready to shoot at start up. (false means must wait reload time before shooting)
		
		angleBottom = 0;  // 0 = facing right
		angleTop = 0;  // 0 = facing right
		
		jl_Tanktop000_00.setVisible(true);
		jl_TankBottom000_00.setVisible(true);
	}
	
	
	public void shoot(double mouseX, double mouseY){
		
		if(canShootCannon == true){
			canShootCannon = false;
		
			// this next block loads the smoke effects in the correct spot based of the angle of the tank's cannon
			if(angleTop == 0.0){
				jl_ShotCloud1.setLocation((int) locX + 151, (int) locY + 34);
				jl_ShotCloud2.setLocation((int) locX + 151, (int) locY + 34);
				jl_ShotCloud3.setLocation((int) locX + 151, (int) locY + 34);
			}
			if(angleTop == 11.25){
				jl_ShotCloud1.setLocation((int) locX + 150, (int) locY + 23);
				jl_ShotCloud2.setLocation((int) locX + 150, (int) locY + 23);
				jl_ShotCloud3.setLocation((int) locX + 150, (int) locY + 23);
			}
			if(angleTop == 22.5){
				jl_ShotCloud1.setLocation((int) locX + 144, (int) locY + 8);
				jl_ShotCloud2.setLocation((int) locX + 144, (int) locY + 8);
				jl_ShotCloud3.setLocation((int) locX + 144, (int) locY + 8);
			}
			if(angleTop == 33.75){
				jl_ShotCloud1.setLocation((int) locX + 130, (int) locY - 8);
				jl_ShotCloud2.setLocation((int) locX + 130, (int) locY - 8);
				jl_ShotCloud3.setLocation((int) locX + 130, (int) locY - 8);
			}
			if(angleTop == 45.0){
				jl_ShotCloud1.setLocation((int) locX + 120, (int) locY - 18);
				jl_ShotCloud2.setLocation((int) locX + 120, (int) locY - 18);
				jl_ShotCloud3.setLocation((int) locX + 120, (int) locY - 18);
			}
			if(angleTop == 56.25){
				jl_ShotCloud1.setLocation((int) locX + 104, (int) locY - 26);
				jl_ShotCloud2.setLocation((int) locX + 104, (int) locY - 26);
				jl_ShotCloud3.setLocation((int) locX + 104, (int) locY - 26);
			}
			if(angleTop == 67.5){
				jl_ShotCloud1.setLocation((int) locX + 87, (int) locY - 35);
				jl_ShotCloud2.setLocation((int) locX + 87, (int) locY - 35);
				jl_ShotCloud3.setLocation((int) locX + 87, (int) locY - 35);
			}
			if(angleTop == 78.75){
				jl_ShotCloud1.setLocation((int) locX + 64, (int) locY - 40);
				jl_ShotCloud2.setLocation((int) locX + 64, (int) locY - 40);
				jl_ShotCloud3.setLocation((int) locX + 64, (int) locY - 40);
			}
			if(angleTop == 90.0){
				jl_ShotCloud1.setLocation((int) locX + 47, (int) locY - 42);
				jl_ShotCloud2.setLocation((int) locX + 47, (int) locY - 42);
				jl_ShotCloud3.setLocation((int) locX + 47, (int) locY - 42);
			}
			if(angleTop == 101.25){
				jl_ShotCloud1.setLocation((int) locX + 24, (int) locY -38);
				jl_ShotCloud2.setLocation((int) locX + 24, (int) locY -38);
				jl_ShotCloud3.setLocation((int) locX + 24, (int) locY -38);
			}
			if(angleTop == 112.5){
				jl_ShotCloud1.setLocation((int) locX + 6, (int) locY - 33);
				jl_ShotCloud2.setLocation((int) locX + 6, (int) locY - 33);
				jl_ShotCloud3.setLocation((int) locX + 6, (int) locY - 33);
			}
			if(angleTop == 123.75){
				jl_ShotCloud1.setLocation((int) locX - 7, (int) locY - 27);
				jl_ShotCloud2.setLocation((int) locX - 7, (int) locY - 27);
				jl_ShotCloud3.setLocation((int) locX - 7, (int) locY - 27);
			}
			if(angleTop == 135.0){
				jl_ShotCloud1.setLocation((int) locX - 23, (int) locY - 18);
				jl_ShotCloud2.setLocation((int) locX - 23, (int) locY - 18);
				jl_ShotCloud3.setLocation((int) locX - 23, (int) locY - 18);
			}
			if(angleTop == 146.25){
				jl_ShotCloud1.setLocation((int) locX - 32, (int) locY - 8);
				jl_ShotCloud2.setLocation((int) locX - 32, (int) locY - 8);
				jl_ShotCloud3.setLocation((int) locX - 32, (int) locY - 8);
			}
			if(angleTop == 157.5){
				jl_ShotCloud1.setLocation((int) locX - 41, (int) locY + 3);
				jl_ShotCloud2.setLocation((int) locX - 41, (int) locY + 3);
				jl_ShotCloud3.setLocation((int) locX - 41, (int) locY + 3);
			}
			if(angleTop == 168.75){
				jl_ShotCloud1.setLocation((int) locX - 47, (int) locY + 20);
				jl_ShotCloud2.setLocation((int) locX - 47, (int) locY + 20);
				jl_ShotCloud3.setLocation((int) locX - 47, (int) locY + 20);
			}
			if(angleTop == 180.0){
				jl_ShotCloud1.setLocation((int) locX - 50, (int) locY + 33);
				jl_ShotCloud2.setLocation((int) locX - 50, (int) locY + 33);
				jl_ShotCloud3.setLocation((int) locX - 50, (int) locY + 33);
			}
			if(angleTop == 191.25){
				jl_ShotCloud1.setLocation((int) locX  - 50, (int) locY + 51);
				jl_ShotCloud2.setLocation((int) locX  - 50, (int) locY + 51);
				jl_ShotCloud3.setLocation((int) locX  - 50, (int) locY + 51);
			}
			if(angleTop == 202.5){
				jl_ShotCloud1.setLocation((int) locX - 43, (int) locY + 64);
				jl_ShotCloud2.setLocation((int) locX - 43, (int) locY + 64);
				jl_ShotCloud3.setLocation((int) locX - 43, (int) locY + 64);
			}
			if(angleTop == 213.75){
				jl_ShotCloud1.setLocation((int) locX - 34, (int) locY + 77);
				jl_ShotCloud2.setLocation((int) locX - 34, (int) locY + 77);
				jl_ShotCloud3.setLocation((int) locX - 34, (int) locY + 77);
			}
			if(angleTop == 225.0){
				jl_ShotCloud1.setLocation((int) locX - 23, (int) locY + 92);
				jl_ShotCloud2.setLocation((int) locX - 23, (int) locY + 92);
				jl_ShotCloud3.setLocation((int) locX - 23, (int) locY + 92);
			}
			if(angleTop == 236.25){
				jl_ShotCloud1.setLocation((int) locX - 12, (int) locY + 99);
				jl_ShotCloud2.setLocation((int) locX - 12, (int) locY + 99);
				jl_ShotCloud3.setLocation((int) locX - 12, (int) locY + 99);
			}
			if(angleTop == 247.5){
				jl_ShotCloud1.setLocation((int) locX + 7, (int) locY + 107);
				jl_ShotCloud2.setLocation((int) locX + 7, (int) locY + 107);
				jl_ShotCloud3.setLocation((int) locX + 7, (int) locY + 107);
			}
			if(angleTop == 258.75){
				jl_ShotCloud1.setLocation((int) locX + 27, (int) locY + 112);
				jl_ShotCloud2.setLocation((int) locX + 27, (int) locY + 112);
				jl_ShotCloud3.setLocation((int) locX + 27, (int) locY + 112);
			}
			if(angleTop == 270.0){
				jl_ShotCloud1.setLocation((int) locX + 50, (int) locY + 113);
				jl_ShotCloud2.setLocation((int) locX + 50, (int) locY + 113);
				jl_ShotCloud3.setLocation((int) locX + 50, (int) locY + 113);
			}
			if(angleTop == 281.25){
				jl_ShotCloud1.setLocation((int) locX + 68, (int) locY + 112);
				jl_ShotCloud2.setLocation((int) locX + 68, (int) locY + 112);
				jl_ShotCloud3.setLocation((int) locX + 68, (int) locY + 112);
			}
			if(angleTop == 292.5){
				jl_ShotCloud1.setLocation((int) locX + 86, (int) locY + 107);
				jl_ShotCloud2.setLocation((int) locX + 86, (int) locY + 107);
				jl_ShotCloud3.setLocation((int) locX + 86, (int) locY + 107);
			}
			if(angleTop == 303.75){
				jl_ShotCloud1.setLocation((int) locX + 102, (int) locY + 100);
				jl_ShotCloud2.setLocation((int) locX + 102, (int) locY + 100);
				jl_ShotCloud3.setLocation((int) locX + 102, (int) locY + 100);
			}
			if(angleTop == 315.0){
				jl_ShotCloud1.setLocation((int) locX + 115, (int) locY + 91);
				jl_ShotCloud2.setLocation((int) locX + 115, (int) locY + 91);
				jl_ShotCloud3.setLocation((int) locX + 115, (int) locY + 91);
			}
			if(angleTop == 326.25){
				jl_ShotCloud1.setLocation((int) locX + 126, (int) locY + 78);
				jl_ShotCloud2.setLocation((int) locX + 126, (int) locY + 78);
				jl_ShotCloud3.setLocation((int) locX + 126, (int) locY + 78);
			}
			if(angleTop == 337.5){
				jl_ShotCloud1.setLocation((int) locX + 134, (int) locY + 67);
				jl_ShotCloud2.setLocation((int) locX + 134, (int) locY + 67);
				jl_ShotCloud3.setLocation((int) locX + 134, (int) locY + 67);
			}
			if(angleTop == 348.75){
				jl_ShotCloud1.setLocation((int) locX + 141, (int) locY + 50);
				jl_ShotCloud2.setLocation((int) locX + 141, (int) locY + 50);
				jl_ShotCloud3.setLocation((int) locX + 141, (int) locY + 50);
			}	
			jl_ShotCloud1.setVisible(true);		
			jl_ShotCloud2.setVisible(true);	
			jl_ShotCloud3.setVisible(true);
			
			
			// this cycles through the 10 shells to allow for enough time
			// to pass so the timer can reset before shooting it again.
			// see projectile.java 'project' function for more details.
			switch(shellNumber){
			case 1: 
				cannonShell01.loadCannonShell(locX, locY, angleTop);
				cannonShell01.project(angleTop);
				cannonShell01.doneDamage = false;
				cannonShell01.jl_cannonShell.setVisible(true);
				break;
			case 2:
				cannonShell02.loadCannonShell(locX, locY, angleTop);
				cannonShell02.project(angleTop);
				cannonShell02.doneDamage = false;
				cannonShell02.jl_cannonShell.setVisible(true);
				break;
			case 3:
				cannonShell03.loadCannonShell(locX, locY, angleTop);
				cannonShell03.project(angleTop);
				cannonShell03.doneDamage = false;
				cannonShell03.jl_cannonShell.setVisible(true);
				break;
			case 4:
				cannonShell04.loadCannonShell(locX, locY, angleTop);
				cannonShell04.project(angleTop);
				cannonShell04.doneDamage = false;
				cannonShell04.jl_cannonShell.setVisible(true);
				break;
			case 5:
				cannonShell05.loadCannonShell(locX, locY, angleTop);
				cannonShell05.project(angleTop);
				cannonShell05.doneDamage = false;
				cannonShell05.jl_cannonShell.setVisible(true);
				break;
			case 6:
				cannonShell06.loadCannonShell(locX, locY, angleTop);
				cannonShell06.project(angleTop);
				cannonShell06.doneDamage = false;
				cannonShell06.jl_cannonShell.setVisible(true);
				break;
			case 7:
				cannonShell07.loadCannonShell(locX, locY, angleTop);
				cannonShell07.project(angleTop);
				cannonShell07.doneDamage = false;
				cannonShell07.jl_cannonShell.setVisible(true);
				break;
			case 8:
				cannonShell08.loadCannonShell(locX, locY, angleTop);
				cannonShell08.project(angleTop);
				cannonShell08.doneDamage = false;
				cannonShell08.jl_cannonShell.setVisible(true);
				break;
			case 9:
				cannonShell09.loadCannonShell(locX, locY, angleTop);
				cannonShell09.project(angleTop);
				cannonShell09.doneDamage = false;
				cannonShell09.jl_cannonShell.setVisible(true);
				break;
			case 10:
				cannonShell10.loadCannonShell(locX, locY, angleTop);
				cannonShell10.project(angleTop);
				cannonShell10.doneDamage = false;
				cannonShell10.jl_cannonShell.setVisible(true);
				break;
			}
			shellNumber++;
			if(shellNumber == 11) shellNumber = 1;
	
			

			
			
			// this timer nest controls the shot smoke fade effect
			// this effect is based off of 3 timers.
			Timer timerShotSmoke_1 = new Timer();
			timerShotSmoke_1.schedule(new TimerTask() {   
				@Override
				public void run() {
					
					jl_ShotCloud1.setVisible(false);  // hide 100% transparency
					
					Timer timerShotSmoke_2 = new Timer();
					timerShotSmoke_2.schedule(new TimerTask() {   
						@Override
						public void run() {
							
							jl_ShotCloud2.setVisible(false);  // hide 66% transparency
							
							Timer timerShotSmoke_3 = new Timer();
							timerShotSmoke_3.schedule(new TimerTask() {   
								@Override
								public void run() {
									
									jl_ShotCloud3.setVisible(false);  // hide 33% transparency
									
								}
							}, 300);
							
						}
					}, 300);
							
				}
			}, 300);
			// end shot smoke fade effect
			
			// this timer throttles down the cannon shooting rate, AKA can only shoot once per cannonFireRate (in milliseconds).
			// reload cannon
			final Timer timerReloadCannon = new Timer();
			timerReloadCannon.schedule(new TimerTask() {   
				@Override
				public void run() {
					canShootCannon = true;
				}
			}, cannonFireRate);
			// end reload cannon
		}
	
	}
	
		
}
